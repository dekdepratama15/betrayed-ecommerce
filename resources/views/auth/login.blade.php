@extends('admin.layouts.app')

@section('content')
    <div class="container sm:px-10">
        <div class="block xl:grid grid-cols-2 gap-4">
            <!-- BEGIN: Login Info -->
            <div class="hidden xl:flex flex-col min-h-screen">
                <a href="" class="-intro-x flex items-center pt-5">
                    <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{asset('user/images/betrayerd.png')}}">
                    <span class="text-white text-lg ml-3"> Admin Betrayed </span>
                </a>
                <div class="my-auto">
                    <img alt="Midone Tailwind HTML Admin Template" class="-intro-x w-1/2 -mt-16" src="{{asset('user/images/betrayedlogo2.png')}}" style="border-radius: 50%">
                    {{-- <div class="-intro-x text-white font-medium text-4xl leading-tight mt-10">
                        A few more clicks to
                        <br>
                        sign in to your account.
                    </div>
                    <div class="-intro-x mt-5 text-lg text-white">Manage all your e-commerce accounts in one place</div> --}}
                </div>
            </div>
            <!-- END: Login Info -->
            <!-- BEGIN: Login Form -->
            <div class="h-screen xl:h-auto flex py-5 xl:py-0 my-10 xl:my-0">
                <div class="my-auto mx-auto xl:ml-20 bg-white xl:bg-transparent px-5 sm:px-8 py-8 xl:p-0 rounded-md shadow-md xl:shadow-none w-full sm:w-3/4 lg:w-2/4 xl:w-auto">
                    <h2 class="intro-x font-bold text-2xl xl:text-3xl text-center xl:text-left">
                        Masuk
                    </h2>
                    {{-- <div class="intro-x mt-2 text-gray-500 xl:hidden text-center">A few more clicks to sign in to your account. Manage all your e-commerce accounts in one place</div> --}}
                    <form action="{{route('login')}}" method="POST" id="login-form">
                        @csrf
                    <div class="intro-x mt-8">
                        <input type="email" name="email" value="{{old('email')}}" class="intro-x login__input input input--lg border border-gray-300 block" placeholder="Email">
                        <input type="password" name="password" required autocomplete="current-password" class="intro-x login__input input input--lg border border-gray-300 block mt-4" placeholder="Password">
                    </div>
                    <div class="intro-x mt-5 xl:mt-8 text-center xl:text-left">
                        {{-- <button type="submit" class="button button--lg w-full xl:w-32 text-white bg-theme-1 xl:mr-3">Login</button> --}}
                        <button type="submit" class="button w-24 inline-block mr-1 mb-2 bg-theme-1 text-white inline-flex items-center"> <span id="login-text" class="pl-4">Login </span> <div id="loading-icon" class="hidden ml-2"><i data-loading-icon="oval" data-color="white" class="w-4 h-4 ml-auto"></i></div>  </button>
                    </div>
                    </form>
                </div>
            </div>
            <!-- END: Login Form -->
        </div>
    </div>
@endsection
