@extends('user.layouts.app')
@section('content')
    <!-- Title page -->
    <section class="bg-img1 txt-center p-lr-15 p-tb-92" style="background-image: url({{ asset('user/images/bannerprofile.jpg') }});">
        <h2 class="ltext-105 cl0 txt-center">
            Profile
        </h2>
    </section>

    <!-- Content page -->
    <section class="bg0 p-t-104 p-b-116">
        <div class="fluid-container">
            <div class="flex-w flex-tr justify-content-center">
                <div class="nav flex-column nav-pills" style="margin-right: 10px" id="v-pills-tab" role="tablist"
                    aria-orientation="vertical">
                    <div class="text-center d-flex flex-column mb-4 align-items-center">
                        @if (isset(auth()->user()->customers->img_profile) && !empty(auth()->user()->customers->img_profile))
                            <img src="{{ asset('upload/' . auth()->user()->customers->img_profile) }}" alt=""
                                width="100px" height="100px" style="border-radius: 20px">
                        @else
                            <img src="{{ 'https://ui-avatars.com/api/?name=' . urlencode(auth()->user()->customers->nama_lengkap) }}"
                                alt="" width="100px" height="100px" style="border-radius: 20px">
                        @endif
                        <b class="my-3">{{ auth()->user()->customers->nama_lengkap }}</b>
                    </div>
                    <a class="nav-link {{ isset($tab) && $tab == 'v-pills-profile' ? 'active' : '' }}" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile"
                        role="tab" aria-controls="v-pills-profile" aria-selected="true">Profile</a>
                    <a class="nav-link {{ isset($tab) && $tab == 'v-pills-dashboard' ? 'active' : '' }}" id="v-pills-dashboard-tab" data-toggle="pill" href="#v-pills-dashboard"
                        role="tab" aria-controls="v-pills-dashboard" aria-selected="false">Dashboard</a>
                    <a class="nav-link {{ isset($tab) && $tab == 'v-pills-ubah-profile' ? 'active' : '' }}" id="v-pills-ubah-profile-tab" data-toggle="pill" href="#v-pills-ubah-profile"
                        role="tab" aria-controls="v-pills-ubah-profile" aria-selected="false">Ubah Profile</a>
                    <a class="nav-link {{ isset($tab) && $tab == 'v-pills-ubah-password' ? 'active' : '' }}" id="v-pills-ubah-password-tab" data-toggle="pill" href="#v-pills-ubah-password"
                        role="tab" aria-controls="v-pills-ubah-password" aria-selected="false">Ganti Password</a>
                </div>
                <div class="size-219 bor10 p-lr-30 p-t-55 p-b-70 p-lr-15-lg w-full">
                    <div class="tab-content" id="v-pills-tabContent">
                        {{-- Profile --}}
                        <div class="tab-pane fade {{ isset($tab) && $tab == 'v-pills-profile' ? 'show active' : '' }}" id="v-pills-profile" role="tabpanel"
                            aria-labelledby="v-pills-profile-tab">
                            <h4 class="mtext-105 text-center">
                                <b>Profile Akun</b>
                            </h4>
                            <div class="col-lg-12">
                                <div class="row">
                                    <div class="col-lg-12 d-flex justify-content-center mb-4 mt-3">
                                        @if (isset(auth()->user()->customers->img_profile) && !empty(auth()->user()->customers->img_profile))
                                            <a class="fancybox" data-caption="{{ auth()->user()->username }}" href="{{asset('upload/'.auth()->user()->customers->img_profile)}}">
                                                <img src="{{ asset('upload/' . auth()->user()->customers->img_profile) }}" alt="" width="100px" height="100px" style="border-radius: 20px">
                                            </a>
                                        @else
                                            <img src="{{ 'https://ui-avatars.com/api/?name=' . urlencode(auth()->user()->customers->nama_lengkap) }}"
                                                alt="" width="100px" height="100px" style="border-radius: 20px">
                                        @endif
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Username</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        {{ auth()->user()->username }}
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Email</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        {{ auth()->user()->email }}
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Nama Lengkap</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        {{ auth()->user()->customers->nama_lengkap }}
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Nama Telepon</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        {{ auth()->user()->customers->telp }}
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Tempat Lahir</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        <span
                                            class="{{ isset(auth()->user()->customers->tempat_lahir) && !empty(auth()->user()->customers->tempat_lahir) ? 'text-black' : 'text-danger' }}">{{ isset(auth()->user()->customers->tempat_lahir) && !empty(auth()->user()->customers->tempat_lahir) ? auth()->user()->customers->tempat_lahir : 'Belum diisi' }}</span>
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-lg-5">
                                        <b>Tanggal lahir</b>
                                    </div>
                                    <div class="col-lg-1">
                                        :
                                    </div>
                                    <div class="col-lg-6">
                                        <span
                                            class="{{ isset(auth()->user()->customers->tanggal_lahir) && !empty(auth()->user()->customers->tanggal_lahir) ? 'text-black' : 'text-danger' }}">{{ isset(auth()->user()->customers->tanggal_lahir) && !empty(auth()->user()->customers->tanggal_lahir) ? date('d-m-Y', strtotime(auth()->user()->customers->tanggal_lahir)) : 'Belum diisi' }}</span>
                                    </div>
                                    <div class="w-100">
                                        <hr>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- End Profile --}}

                        {{-- Ubah Profile --}}
                        <div class="tab-pane fade {{ isset($tab) && $tab == 'v-pills-ubah-profile' ? 'show active' : '' }}" id="v-pills-ubah-profile" role="tabpanel" aria-labelledby="v-pills-ubah-profile-tab">
                            <form action="{{ route('ubah.profile',auth()->user()->id) }}" method="post" enctype="multipart/form-data" autocomplete="off">
                                @method('PUT')
                                @csrf
                                <h4 class="mtext-105 cl2 txt-center p-b-30">
                                    <b>Ubah Profile Akun</b>
                                </h4>

                                <div class="col-lg-12">
                                    <div class="row mb-3">
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">Username</label>
                                            <input type="text" class="form form-control form-control-sm" value="{{ auth()->user()->username }}" name="username">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">Email</label>
                                            <input type="text" class="form form-control form-control-sm" value="{{ auth()->user()->email }}" name="email">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">Nama lengkap</label>
                                            <input type="text" class="form form-control form-control-sm" value="{{ auth()->user()->customers->nama_lengkap }}" name="nama_lengkap">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">No telepon</label>
                                            <input type="text" class="form form-control form-control-sm" value="{{ auth()->user()->customers->telp }}" name="telp">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">Tempat lahir</label>
                                            <input type="text" class="form form-control form-control-sm" placeholder="Masukkan tempat lahir" value="{{ auth()->user()->customers->tempat_lahir }}" name="tempat_lahir">
                                        </div>
                                        <div class="col-lg-6">
                                            <label for="" class="form-label form-control-label">Tanggal lahir</label>
                                            <input type="date" class="form form-control form-control-sm" value="{{ date('Y-m-d', strtotime(auth()->user()->customers->tanggal_lahir)) }}" name="tanggal_lahir">
                                        </div>
                                    </div>

                                    <div class="row mb-3">
                                        <div class="col-lg-12">
                                            <label for="" class="form-label form-control-label">Foto profile</label>
                                            <input type="file" class="form form-control form-control-sm" name="img_profile">
                                        </div>
                                    </div>


                                    <div class="row mt-4">
                                        <div class="col-lg-12 text-right mt-4">
                                            <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                </div>


                            </form>
                        </div>
                        {{-- End Ubah Profile --}}

                        {{-- Ubah Password --}}
                        <div class="tab-pane fade {{ isset($tab) && $tab == 'v-pills-ubah-password' ? 'show active' : '' }}" id="v-pills-ubah-password" role="tabpanel" aria-labelledby="v-pills-ubah-password-tab">
                            <form action="{{ route('ubah.passwordUser',auth()->user()->id) }}" method="POST" autocomplete="off">
                                @method('PUT')
                                @csrf
                                <h4 class="mtext-105 cl2 txt-center p-b-30">
                                    <b>Ubah Passowrd <span class="text-danger">{{ auth()->user()->username }}</span></b>
                                </h4>

                                <div class="col-lg-12">
                                    <div class="row mb-3">
                                        <div class="col-lg-12">
                                            <label for="" class="form-label form-control-label">Password lama</label>
                                            <input type="password" class="form form-control form-control-sm" value="" name="password_lama" placeholder="Masukkan password lama" >
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-lg-12">
                                            <label for="" class="form-label form-control-label">Password baru</label>
                                            <input type="password" class="form form-control form-control-sm" value="" name="password" placeholder="Masukkan password baru">
                                        </div>
                                    </div>
                                    <div class="row mb-3">
                                        <div class="col-lg-12">
                                            <label for="" class="form-label form-control-label">Konfirmasi password baru</label>
                                            <input type="password" class="form form-control form-control-sm" value="" name="password_konfirmasi" placeholder="Masukkan password konfirmasi" >
                                        </div>
                                    </div>

                                    <div class="row mt-4">
                                        <div class="col-lg-12 text-right mt-4">
                                            <button class="btn btn-primary btn-sm" type="submit">Simpan</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                        {{-- Ubah Password --}}

                        {{-- Dashboard --}}
                        <div class="tab-pane fade {{ isset($tab) && $tab == 'v-pills-dashboard' ? 'show active' : '' }}" id="v-pills-dashboard" role="tabpanel" aria-labelledby="v-pills-dashboard-tab">
                            <div class="m-lr-0-xl">
                                <div class="wrap-table-shopping-cart">
                                    <table class="table-shopping-cart">
                                        <thead>
                                            <tr class="table_head">
                                                <th class="column-1">Invoice</th>
                                                <th class="column-1">Alamat</th>
                                                <th class="column-1">Total</th>
                                                <th class="column-1">Status</th>
                                                <th class="column-1">Aksi</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @if (count($orders) > 0)
                                                @foreach ($orders as $order)
                                                    <tr>
                                                        <td class="column-1 py-4">{{ $order->invoice }}</td>
                                                        <td class="column-1 py-4" >{{ strlen($order->alamat) > 10 ? substr($order->alamat, 0 , 10).' ...' : $order->alamat }}</td>
                                                        <td class="column-1 py-4" >Rp {{ number_format($order->total,0,',','.') }}</td>
                                                        <td class="column-1 py-4" >
                                                            @if ($order->status == 'pending')
                                                                <span class="badge badge-primary p-2">Pending</span>
                                                            @elseif($order->status == 'menunggu_pembayaran')
                                                                <span class="badge badge-warning p-2">Lakukan Pembayaran</span>
                                                            @elseif($order->status == 'menunggu_persetujuan')
                                                                <span class="badge badge-warning p-2">Menunggu Persetujuan</span>
                                                            @elseif($order->status == 'terbayar')
                                                                <span class="badge badge-success p-2">Sudah terbayar</span>
                                                            @elseif($order->status == 'pengiriman')
                                                                <span class="badge badge-info p-2">Sedang Pengiriman</span>
                                                            @elseif($order->status == 'diterima')
                                                                <span class="badge badge-success p-2">Sudah Diterima</span>
                                                            @elseif($order->status == 'dibatalkan')
                                                                <span class="badge badge-danger p-2">Dibatalkan</span>
                                                            @elseif($order->status == 'ditolak')
                                                                <span class="badge badge-danger p-2">Ditolak</span>
                                                            @endif
                                                        </td>
                                                        <td class="column-1 py-4" >
                                                            <a href="{{ route('detailOrder.edit',$order->id) }}" class="btn btn-sm btn-success">Detail</a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                            @else
                                            <tr>
                                                <td class="column-1 py-4 text-center" colspan="5">Tidak Memiliki Data Pesanan</td>
                                            </tr>
                                            @endif
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                        {{-- Dashboard --}}
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection

@section('js-content')
<script>
    $(document).ready(function(){
        $('#v-pills-tab a[href="#{{ session('tab') }}"]').tab('show');
    });
</script>
@endsection
