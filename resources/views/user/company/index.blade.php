@extends('user.layouts.app')

@section('content')

<div class="bg0 p-t-120 p-b-140">
    <div class="container">
        <div class="text-center">
            <img src="{{ asset('upload/'.$company[0]->logo) }}" alt="" height="300px" height="300px">
            <div class="mt-4">
                <h1>{{ $company[0]->nama }}</h1>
                <div class="text-center d-flex justify-content-center" style="margin-left: 100px">
                    <hr style="width: 130px; border:none;background-color:black;height:5px">
                </div>
            </div>
        </div>
        <div class="text-justify mt-4">
            <p style="text-indent: 50px;line-height: 2rem">{{$company[0]->deskripsi}}</p>
        </div>
        <div class="row" style="margin-top: 5rem">
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center" style="font-weight: bold" >VISI</h4>
                        <p class="text-justify">{{ $company[0]->visi }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="card">
                    <div class="card-body">
                        <h4 class="text-center" style="font-weight: bold" >MISI</h4>
                        <p class="text-justify">{{ $company[0]->misi }}</p>
                    </div>
                </div>
            </div>
        </div>

        <div style="margin-top: 5rem">
            <p style="font-weight: bold;font-size:26px">Hubungi</p>
        </div>
        <div class="row mt-4 text-center">
            <div class="col-lg-6">
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('user/images/location.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->alamat }}</p>
                    </div>
                </div>
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('user/images/telephone-call.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->telp }}</p>
                    </div>
                </div>
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('user/images/instagram.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->instagram }}</p>
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('user/images/facebook.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->facebook }}</p>
                    </div>
                </div>
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('shopee.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->shopee }}</p>
                    </div>
                </div>
                <div class="pt-2 d-flex align-content-center">
                    <div>
                        <img src="{{ asset('tokopedia.png') }}" style="max-width: 32px;" alt="">
                    </div>
                    <div class="pl-3">
                        <p style="font-size: 18px">{{ $company[0]->tokopedia }}</p>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
