@extends('user.layouts.app')

@section('content')
<!-- Slider -->
<section class="section-slide">
  <div class="wrap-slick1">
    <div class="slick1">
      <div class="item-slick1" style="background-image: url({{asset('user/images/slide-1.jpg')}});">
        <div class="container h-full">
          <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
            <div class="layer-slick1 animated visible-false" data-appear="fadeInDown" data-delay="0">
              <span class="ltext-101 cl2 respon2">
                SELAMAT DATANG
              </span>
            </div>

            <div class="layer-slick1 animated visible-false" data-appear="fadeInUp" data-delay="800">
              <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                di BETRAYED
              </h2>
            </div>

            <div class="layer-slick1 animated visible-false" data-appear="zoomIn" data-delay="1600">
              <a href="{{ route('shop.user') }}" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                Belanja Sekarang
              </a>
            </div>
          </div>
        </div>
      </div>

      <div class="item-slick1" style="background-image: url({{asset('user/images/slide-2.jpg')}});">
        <div class="container h-full">
          <div class="flex-col-l-m h-full p-t-100 p-b-30 respon5">
            <div class="layer-slick1 animated visible-false" data-appear="rollIn" data-delay="0">
              <span class="ltext-101 cl2 respon2">
                BAJU DENGAN KUALITAS
              </span>
            </div>

            <div class="layer-slick1 animated visible-false" data-appear="lightSpeedIn" data-delay="800">
              <h2 class="ltext-201 cl2 p-t-19 p-b-43 respon1">
                YANG TERBAIK
              </h2>
            </div>

            <div class="layer-slick1 animated visible-false" data-appear="slideInUp" data-delay="1600">
              <a href="{{ route('shop.user') }}" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04">
                Belanja Sekarang
              </a>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>


<!-- Banner -->
<div class="sec-banner bg0 p-t-80 p-b-50">
  <div class="container">
    <div class="text-center mb-4">
        <h1 class="" style="font-weight: bold">Pelayanan</h1>
    </div>
    <div class="row">

    <div class="col-lg-4">
        <div class="card">
            <div class="card-body text-center">
                <div>
                    <img src="{{ asset('user/images/laundry.png') }}" width="200px" alt="">
                </div>
                <div class="mt-2">
                    <h4 style="font-weight: bold">Menggunakan Kualitas</h4>
                    <p style="font-size: 20px">Kain Terbaik</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body text-center">
                <div>
                    <img src="{{ asset('user/images/express-delivery.png') }}" width="200px" alt="">
                </div>
                <div class="mt-2">
                    <h4 style="font-weight: bold">Pengiriman</h4>
                    <p style="font-size: 20px">Cepat</p>
                </div>
            </div>
        </div>
    </div>
    <div class="col-lg-4">
        <div class="card">
            <div class="card-body text-center">
                <div>
                    <img src="{{ asset('user/images/clock.png') }}" width="200px" alt="">
                </div>
                <div class="mt-2">
                    <h4 style="font-weight: bold">Terhubung</h4>
                    <p style="font-size: 20px">24 Jam</p>
                </div>
            </div>
        </div>
    </div>
    </div>
  </div>
</div>


<!-- Product -->
<section class="bg0 p-t-23 p-b-50">
  <div class="container">
    <div class="p-b-10">
      <h3 class="ltext-103 cl5">
        Produk
      </h3>
    </div>


    <div class="row isotope-grid">
        @foreach ($produks as $produk)
            @if ($produk->status == 'aktif')
                <div>
                    <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                        <!-- Block2 -->
                        <div class="block2">
                        <div class="block2-pic hov-img0">
                            <div class="img-container">
                                <img src="{{asset('upload/'.$produk->produk_variants[0]->img)}}"alt="IMG-PRODUCT">
                                @if ($produk->produk_variants->where('stok')->count() === 0)
                                    <span class="badge bg-danger">Stok Kosong</span>
                                @endif
                            </div>
                        </div>

                        <div class="block2-txt flex-w flex-t p-t-14">
                            <div class="block2-txt-child1 flex-col-l ">
                            <a href="product-detail.html" class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                {{ $produk->nama_produk }}
                            </a>

                            <span class="stext-105 cl3">
                                Rp. {{number_format($produk->price,0,',','.')}}
                            </span>
                            </div>

                            <div class="block2-txt-child2 flex-r p-t-3">
                            <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2 ">
                                <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#exampleModal{{ $produk->id }}"><i class="zmdi zmdi-eye"></i></button>
                            </a>
                            </div>
                        </div>
                        </div>
                    </div>
                </div>
            @endif
        @endforeach

    </div>

    <!-- Load more -->
    <div class="d-flex justify-content-center w-100" style="margin-top: 5rem">
        <a href="{{ route('shop.user') }}" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04" style="text-decoration: none">
            Semua Produk
        </a>
    </div>
  </div>
</section>

<section class="bg0 p-t-23">
    <div class="container">
      <div class="p-b-10">
        <h3 class="ltext-103 cl5">
          Blog
        </h3>
        </div>

        <div class="flex-w flex-sb-m p-b-52">
            <div class="row">
                @foreach ($blogs as $blog)
                    @if ($blog->status == 'publish')
                        <div class="col-lg-4">
                            <div class="card">
                                <img class="card-img-top" src="{{ 'upload/'.$blog->blog_medias[0]->media }}" width="320px" height="320px" alt="Card image cap">
                                <div class="card-body">
                                <h5 class="card-title">{{ $blog->judul }}</h5>
                                <p class="card-text text-justify">
                                    @if (strlen($blog->deskripsi) > 20)
                                        {{ substr($blog->deskripsi, 0, 100) }}...
                                    @else
                                        {{ $blog->deskripsi }}
                                    @endif
                                </p>
                                <a href="{{ route('blog-detail',$blog->id) }}" class="btn text-light" style="background-color: #6c7ae0">Selengkapnya</a>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
            <div class="d-flex justify-content-center w-100" style="margin-top: 5rem">
                <a href="{{ route('blog') }}" class="flex-c-m stext-101 cl0 size-101 bg1 bor1 hov-btn1 p-lr-15 trans-04" style="text-decoration: none">
                    Lihat Semua
                </a>
            </div>
        </div>
    </div>
  </section>

	<!-- Modal1 -->
    @foreach ($produks as $key => $produk)
        <!-- Modal -->
        <div class="modal fade" id="exampleModal{{ $produk->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" style="padding: 50px 30px">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div id="carouselExampleControls{{ $key }}" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        @foreach ($produk->produk_variants as $index => $produk_variant)
                                            <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                                <img class="d-block" src="{{asset('upload/'.$produk_variant->img)}}" width="100%" height="350px" alt="First slide">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls{{ $key }}" role="button" data-slide="prev">
                                      <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: black"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls{{ $key }}" role="button" data-slide="next">
                                      <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: black"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                  </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <h4>{{ $produk->nama_produk }}</h4>
                                </div>
                                <div class="mt-4">
                                    <p class="text-justify">{{ $produk->deskripsi }}</p>
                                </div>
                                <div>
                                    <b>
                                        Rp. {{number_format($produk->price,0,',','.')}}
                                    </b>
                                </div>
                                @if (auth()->user())
                                    @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                        <div class="mt-3">
                                            <select name="" id="ukuran_cart_{{ $key }}" class="form form-control">
                                                <option value="" selected>Pilih ukuran</option>
                                                @foreach ($produk->produk_variants as $produk_variant)
                                                    @if ($produk_variant->stok > 0)
                                                        <option value="{{ $produk_variant->id }}" data-stok="{{ $produk_variant->stok }}">{{ $produk_variant->nama_variant }} - {{ $produk_variant->stok }}</option>
                                                    @else
                                                        <div class="mt-3">
                                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        @foreach ($produk->produk_variants as $produk_variant)
                                            <div class="mt-3">
                                                <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                        <div class="mt-3">
                                            <input type="text" class="form form-control" placeholder="Masukkan Jumlah" id="jumlah_cart_{{ $key }}">
                                        </div>
                                    @endif
                                @else
                                  @foreach ($produk->produk_variants as $produk_variant)
                                    @if ($produk_variant->stok > 0)
                                        <div class="mt-3">
                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - {{ $produk_variant->stok }}</p>
                                        </div>
                                    @else
                                        <div class="mt-3">
                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                        </div>
                                    @endif
                                  @endforeach
                                @endif
                                <div class="mt-4 d-flex">
                                      @if (auth()->user())
                                        @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                            <button class="btn text-light add-to-cart" style="background-color: #6c7ae0;border-radius:20px" data-produk_id="{{ $produk->id }}" data-key="{{ $key }}">Tambah Ke Keranjang</button>
                                        @endif
                                      @endif
                                      <a class="btn btn-secondary ml-2" style="border-radius: 20px" href="{{ route('detail.produk',['id'=>$produk->id,'slug' => $produk->slug]) }}">Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    @endforeach

    {{-- Modal Voucher --}}
    @if (!empty($vouchers) && !isset($voucher))
        <div class="modal" tabindex="-1" role="dialog" id="modalVoucher">
            <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body">
                    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
                        <div class="carousel-inner">
                            @foreach ($vouchers as $index => $voucher)
                                @if ($voucher->jumlah > 0)
                                    <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                        <img class="d-block w-100" src="{{ asset('upload/'.$voucher->gambar_voucher) }}" width="480px" height="480px" alt="Slide {{ $index + 1 }}">
                                        <div class="mt-4" >
                                            <span style="font-size: 16px">Kode Voucher : <b style="font-size: 18px">{{ $voucher->nama }}</b></span>
                                        </div>
                                        <div class="mt-4">
                                            <p class="text-justify" style="text-indent: 30px" >{{ $voucher->deskripsi }}</p>
                                        </div>
                                    </div>
                                @endif
                            @endforeach
                        </div>
                        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="sr-only">Previous</span>
                        </a>
                        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="sr-only">Next</span>
                        </a>
                    </div>
                </div>
            </div>
            </div>
        </div>
    @endif

@endsection

@section('js-content')
    @if (count($vouchers) > 0)
        <script>
            $(document).ready(function () {
                $('#modalVoucher').modal('show');
            })
        </script>
    @endif
@endsection

