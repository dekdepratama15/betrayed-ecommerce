@extends('user.layouts.app')

@section('content')

<div class="bg0 p-t-120 p-b-140" >
    <div class="container">
        <div class="row">
            @if (count($blogs) > 0)
                @foreach ($blogs as $blog)
                    <div class="col-lg-6 mt-5">
                        <div class="card">
                            <img class="card-img-top" src="{{ 'upload/'.$blog->blog_medias[0]->media }}" alt="Card image cap">
                            <div class="card-body">
                            <h5 class="card-title">{{ $blog->judul }}</h5>
                            <p class="card-text text-justify">
                                @if (strlen($blog->deskripsi) > 20)
                                    {{ substr($blog->deskripsi, 0, 100) }}...
                                @else
                                    {{ $blog->deskripsi }}
                                @endif
                            </p>
                            <a href="{{ route('blog-detail',$blog->id) }}" class="btn text-light" style="background-color: #6c7ae0">Selengkapnya</a>
                            </div>
                        </div>
                    </div>
                @endforeach
            @else
                <div class="col-lg-12 mt-5">
                    <div class="card" style="height: 400px">
                        <div class="card-body d-flex align-items-center justify-content-center">
                            <h5 class="card-title">Tidak Ada Blog</h5>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>
</div>
@endsection
