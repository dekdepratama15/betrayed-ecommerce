@extends('user.layouts.app')

@section('content')

<div class="bg0 p-t-100 p-b-140">
    <div class="fluid-container">
        <div id="carouselExampleControls" class="carousel slide" data-ride="carousel">
            <div class="carousel-inner">
                @foreach ($blog->blog_medias as $index => $blog_media)
                <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                    <img class="d-block w-100" height="720px" src="{{ asset('upload/'.$blog_media->media) }}" style="object-fit: cover;" alt="First slide">
                </div>
                @endforeach
            </div>
            <a class="carousel-control-prev" href="#carouselExampleControls" role="button" data-slide="prev">
              <span class="carousel-control-prev-icon" aria-hidden="true"></span>
              <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#carouselExampleControls" role="button" data-slide="next">
              <span class="carousel-control-next-icon" aria-hidden="true"></span>
              <span class="sr-only">Next</span>
            </a>
          </div>
          <div class="mx-4 my-4">
            <div class="mt-4">
                <p style="font-weight: bold;font-size:36px">{{ $blog->judul }}</p>
            </div>
            <div>
                <p class="text-justify">{{ $blog->deskripsi }}</p>
            </div>
          </div>
    </div>
</div>
@endsection
