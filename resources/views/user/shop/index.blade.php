@extends('user.layouts.app')

@section('content')

<div class="bg0 p-t-120 p-b-140">
    <div class="container">
        <div class="flex-w flex-sb-m p-b-52">
            <div class="flex-w flex-l-m filter-tope-group m-tb-10">
                <button class="stext-106 cl6 hov1 trans-04 m-r-32 m-tb-5 " style="color: black" data-filter="*">
                    Semua Produk
                </button>
            </div>

            <div class="flex-w flex-c-m m-tb-10">

                <div class="flex-c-m stext-106 cl6 size-105 bor4 pointer hov-btn3 trans-04 m-tb-4 js-show-search">
                    <i class="icon-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-search"></i>
                    <i class="icon-close-search cl2 m-r-6 fs-15 trans-04 zmdi zmdi-close dis-none"></i>
                    Search
                </div>
            </div>

            <!-- Search product -->
            <div class="dis-none panel-search w-full p-t-10 p-b-15">
                <div class="bor8 dis-flex p-l-15">
                    <button class="size-113 flex-c-m fs-16 cl2 hov-cl1 trans-04">
                        <i class="zmdi zmdi-search"></i>
                    </button>
                    <form action="" method="get" class="position-relative">
                        <input class="mtext-107 cl2 size-114 plh2 p-r-15" type="text" value="{{ $search }}" name="search" autocomplete="off" placeholder="Search">
                    </form> 
                </div>
            </div>
        </div>
        @if (count($produks) > 0)
            <div class="row isotope-grid">
                @foreach ($produks as $produk)
                    @if ($produk->status == 'aktif')
                        <div class="col-sm-6 col-md-4 col-lg-3 p-b-35 isotope-item women">
                            <!-- Block2 -->
                            <div class="block2">
                                <div class="block2-pic hov-img0">
                                    <div class="img-container">
                                        <img src="{{asset('upload/'.$produk->produk_variants[0]->img)}}"alt="IMG-PRODUCT">
                                        @if ($produk->produk_variants->where('stok')->count() === 0)
                                            <span class="badge bg-danger">Stok Kosong</span>
                                        @endif
                                    </div>
                                </div>

                                <div class="block2-txt flex-w flex-t p-t-14">
                                    <div class="block2-txt-child1 flex-col-l ">
                                    <span class="stext-104 cl4 hov-cl1 trans-04 js-name-b2 p-b-6">
                                        {{ $produk->nama_produk }}
                                    </span>

                                    <span class="stext-105 cl3">
                                        Rp. {{number_format($produk->price,0,',','.')}}
                                    </span>
                                    </div>

                                    <div class="block2-txt-child2 flex-r p-t-3">
                                    <a href="#" class="btn-addwish-b2 dis-block pos-relative js-addwish-b2">
                                        <button type="button" class="btn btn-sm btn-secondary" data-toggle="modal" data-target="#exampleModal{{ $produk->id }}"><i class="zmdi zmdi-eye"></i></button>
                                    </a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endif
                @endforeach
            </div>
        @else
        <div class="flex-w flex-sb-m p-b-52">
            <a class="mtext-107 cl2 size-114 plh2 p-r-15" style="color: black; text-align:center;" data-filter="*">
                Tidak terdapat data Produk
            </a>
        </div>
        @endif
        <div class="row">
            <div class="col-12 mt-2 mt-md-4">
                <ul class="pagination pagination_style1 justify-content-center">
                   {{ $produks->links() }}
                </ul>
            </div>
        </div>
    </div>

    <!-- Modal1 -->
    @foreach ($produks as $key => $produk)
        <!-- Modal -->
        <div class="modal fade" id="exampleModal{{ $produk->id }}" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
            <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
            <div class="modal-content">
                <div class="modal-body" style="padding: 50px 30px">
                    <div class="col-lg-12">
                        <div class="row">
                            <div class="col-lg-6">
                                <div id="carouselExampleControls{{ $key }}" class="carousel slide" data-ride="carousel">
                                    <div class="carousel-inner">
                                        @foreach ($produk->produk_variants as $index => $produk_variant)
                                            <div class="carousel-item {{ $index === 0 ? 'active' : '' }}">
                                                <img class="d-block" src="{{asset('upload/'.$produk_variant->img)}}" width="100%" height="350px" alt="First slide">
                                            </div>
                                        @endforeach
                                    </div>
                                    <a class="carousel-control-prev" href="#carouselExampleControls{{ $key }}" role="button" data-slide="prev">
                                      <span class="carousel-control-prev-icon" aria-hidden="true" style="background-color: black"></span>
                                      <span class="sr-only">Previous</span>
                                    </a>
                                    <a class="carousel-control-next" href="#carouselExampleControls{{ $key }}" role="button" data-slide="next">
                                      <span class="carousel-control-next-icon" aria-hidden="true" style="background-color: black"></span>
                                      <span class="sr-only">Next</span>
                                    </a>
                                  </div>
                            </div>
                            <div class="col-lg-6">
                                <div>
                                    <h4>{{ $produk->nama_produk }}</h4>
                                </div>
                                <div class="mt-4">
                                    <p class="text-justify">{{ $produk->deskripsi }}</p>
                                </div>
                                <div>
                                    <b>
                                        Rp. {{number_format($produk->price,0,',','.')}}
                                    </b>
                                </div>
                                @if (auth()->user())
                                    @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                        <div class="mt-3">
                                            <select name="" id="ukuran_cart_{{ $key }}" class="form form-control">
                                                <option value="" selected>Pilih ukuran</option>
                                                @foreach ($produk->produk_variants as $produk_variant)
                                                    @if ($produk_variant->stok > 0)
                                                        <option value="{{ $produk_variant->id }}" data-stok="{{ $produk_variant->stok }}">{{ $produk_variant->nama_variant }} - {{ $produk_variant->stok }}</option>
                                                    @else
                                                        <div class="mt-3">
                                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                                        </div>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                    @else
                                        @foreach ($produk->produk_variants as $produk_variant)
                                            <div class="mt-3">
                                                <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                            </div>
                                        @endforeach
                                    @endif
                                    @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                        <div class="mt-3">
                                            <input type="text" class="form form-control" placeholder="Masukkan Jumlah" id="jumlah_cart_{{ $key }}">
                                        </div>
                                    @endif
                                @else
                                  @foreach ($produk->produk_variants as $produk_variant)
                                    @if ($produk_variant->stok > 0)
                                        <div class="mt-3">
                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - {{ $produk_variant->stok }}</p>
                                        </div>
                                    @else
                                        <div class="mt-3">
                                            <p><span style="color: #6c7ae0">{{ $produk_variant->nama_variant }}</span> - Stok Kosong</p>
                                        </div>
                                    @endif
                                  @endforeach
                                @endif
                                <div class="mt-4 d-flex">
                                      @if (auth()->user())
                                        @if ($produk->produk_variants->where('stok', '>', 0)->count() > 0)
                                            <button class="btn text-light add-to-cart" style="background-color: #6c7ae0;border-radius:20px" data-produk_id="{{ $produk->id }}" data-key="{{ $key }}">Tambah Ke Keranjang</button>
                                        @endif
                                      @endif
                                      <a class="btn btn-secondary ml-2" style="border-radius: 20px" href="{{ route('detail.produk',['id'=>$produk->id,'slug' => $produk->slug]) }}">Detail</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    @endforeach

</div>
@endsection
