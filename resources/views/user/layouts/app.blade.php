<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Home</title>
  <!--===============================================================================================-->
	<link rel="icon" type="image/png" href="images/icons/favicon.png"/>
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/bootstrap/css/bootstrap.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/fonts/iconic/css/material-design-iconic-font.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/fonts/linearicons-v1.0.0/icon-font.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/animate/animate.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/css-hamburgers/hamburgers.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/animsition/css/animsition.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/select2/select2.min.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/daterangepicker/daterangepicker.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/slick/slick.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/MagnificPopup/magnific-popup.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/vendor/perfect-scrollbar/perfect-scrollbar.css')}}">
  <!--===============================================================================================-->
    <link rel="stylesheet" type="text/css" href="{{asset('user/css/util.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('user/css/main.css')}}">
    {{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.2.1/dist/css/bootstrap.min.css" integrity="sha384-GJzZqFGwb1QTTN6wy59ffF1BuGJpLSa9DkKMp0DgiMDm4iYMj70gZWKYbI706tWS" crossorigin="anonymous"> --}}
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
    <link rel="stylesheet" href="https://unpkg.com/leaflet@1.9.4/dist/leaflet.css" integrity="sha256-p4NxAoJBhIIN+hmNHrzRCf9tD/miZyoHS5obTRR9BMY=" crossorigin="" />
  <!--===============================================================================================-->
</head>
<body class="animsition">
    @include('user.layouts.navbar')
    @yield('content')
    @include('user.layouts.footer')

    <input type="hidden" id="route-global" value="{{ url('/') }}">
  <!--===============================================================================================-->
	{{-- <script src="{{asset('user/vendor/jquery/jquery-3.2.1.min.js')}}"></script> --}}
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js?v2" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/animsition/js/animsition.min.js')}}"></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/bootstrap/js/popper.js')}}"></script>
    <script src="{{asset('user/vendor/bootstrap/js/bootstrap.min.js')}}"></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/select2/select2.min.js')}}"></script>
    <script>
      $(".js-select2").each(function(){
        $(this).select2({
          minimumResultsForSearch: 20,
          dropdownParent: $(this).next('.dropDownSelect2')
        });
      })
    </script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/daterangepicker/moment.min.js')}}"></script>
    <script src="{{asset('user/vendor/daterangepicker/daterangepicker.js')}}"></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/slick/slick.min.js')}}"></script>
    <script src="{{asset('user/js/slick-custom.js')}}"></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/parallax100/parallax100.js')}}"></script>
    <script>
          $('.parallax100').parallax100();
    </script>
    <script src="https://unpkg.com/leaflet@1.9.4/dist/leaflet.js" integrity="sha256-20nQCchB9co0qIjJZRGuk2/Z9VM+kNiyxNV1lvTlZBo=" crossorigin=""></script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/MagnificPopup/jquery.magnific-popup.min.js')}}"></script>
    <script>
      $('.gallery-lb').each(function() { // the containers for all your galleries
        $(this).magnificPopup({
              delegate: 'a', // the selector for gallery item
              type: 'image',
              gallery: {
                enabled:true
              },
              mainClass: 'mfp-fade'
          });
      });
    </script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/isotope/isotope.pkgd.min.js')}}"></script>
  <!--===============================================================================================-->
    {{-- <script src="{{asset('user/vendor/sweetalert/sweetalert.min.js')}}"></script> --}}
    <script>
      $('.js-addwish-b2').on('click', function(e){
        e.preventDefault();
      });

      $('.js-addwish-b2').each(function(){
        var nameProduct = $(this).parent().parent().find('.js-name-b2').html();
        $(this).on('click', function(){
          swal(nameProduct, "is added to wishlist !", "success");

          $(this).addClass('js-addedwish-b2');
          $(this).off('click');
        });
      });

      $('.js-addwish-detail').each(function(){
        var nameProduct = $(this).parent().parent().parent().find('.js-name-detail').html();

        $(this).on('click', function(){
          swal(nameProduct, "is added to wishlist !", "success");

          $(this).addClass('js-addedwish-detail');
          $(this).off('click');
        });
      });

      /*---------------------------------------------*/

      $('.js-addcart-detail').each(function(){
        var nameProduct = $(this).parent().parent().parent().parent().find('.js-name-detail').html();
        $(this).on('click', function(){
          swal(nameProduct, "is added to cart !", "success");
        });
      });

    </script>
  <!--===============================================================================================-->
    <script src="{{asset('user/vendor/perfect-scrollbar/perfect-scrollbar.min.js')}}"></script>
    <script>
      $('.js-pscroll').each(function(){
        $(this).css('position','relative');
        $(this).css('overflow','hidden');
        var ps = new PerfectScrollbar(this, {
          wheelSpeed: 1,
          scrollingThreshold: 1000,
          wheelPropagation: false,
        });

        $(window).on('resize', function(){
          ps.update();
        })
      });
    </script>
  <!--===============================================================================================-->
    <script src="{{asset('user/js/main.js')}}"></script>

    {{-- <script>
      function showLoadingIndicator() {
          $('#login-text').text('Memuat...');
          $('#loading-icon').removeClass('d-none');
      }

      function hideLoadingIndicator() {
          $('#login-text').text('Daftar');
          $('#loading-icon').addClass('d-none');
      }
      $('#form-register').on('submit', function (e) {
          if (e.target && e.target.tagName === 'FORM') {
              showLoadingIndicator();

              const formData = new FormData(e.target);

              $.ajax({
                  type: 'POST',
                  url: e.target.action,
                  data: formData,
                  processData: false,
                  contentType: false,
                  success: function (data) {
                    Swal.fire(
                        'Berhasil!',
                        data.message,
                        'success'
                    ).then(() => {
                        location.reload();
                    });
                  },
                  complete: function () {
                    hideLoadingIndicator();
                  }
              });

              e.preventDefault();
          }
      });
  </script> --}}

  <script>
    $(document).ready(function(){
        $('.fancybox').fancybox();
        getCart();
    })
  </script>

  @if(session('success'))
  <script>
  $(document).ready(function() {
      Swal.fire(
          'Berhasil!',
          '{{session("success")}}',
          'success'
      );
  });
  </script>
  @endif

  @if(session('warning'))
  <script>
  $(document).ready(function() {
      Swal.fire(
          'Gagal!',
          '{{session("warning")}}',
          'warning'
      );
  });
  </script>
  @endif

  <script>
    function getCart() {
        $.ajax({
            type: "get",
            url: "{{ route('get-cart') }}",
            data: {},
            success: function (response) {
                displayDataCart(response.data);
            }
        });

        function displayDataCart(carts){
            var text = '';
            var total = '';
            var total_produk = 0;
            carts.map((data) => {
                text += `
                <ul class="header-cart-wrapitem w-full">
                    <li class="header-cart-item flex-w flex-t m-b-12">
                        <div class="header-cart-item-img">
                        <img src="${$('#route-global').val()}/upload/${data.produk_variant.img}" alt="IMG">
                        </div>

                        <div class="header-cart-item-txt p-t-8">
                        <a href="#" class="header-cart-item-name hov-cl1 trans-04">
                            ${data.produk_variant.produk.nama_produk}
                        </a>
                        <span class="header-cart-item-info">
                            ${data.produk_variant.nama_variant}
                        </span>
                        <span class="header-cart-item-info">
                            ${data.qty} x Rp. ${formatRupiah(data.produk_variant.produk.price)}
                        </span>
                        </div>
                    </li>
                </ul>
                `;
                total_produk += parseFloat(data.total);
            });

            $('#body-cart').html(text);
            $('#total_produk').html('Total: Rp.'+formatRupiah(total_produk));
            $('#cart-notif').attr('data-notify', carts.length)
        }
    }

    function formatRupiah(str) {
        return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
    }

    $('.add-to-cart').on('click', function () {
      var key = $(this).data('key');
      var produk_id = $(this).data('produk_id');
      var jumlah = $('#jumlah_cart_' + key).val();
      var ukuran = $('#ukuran_cart_' + key).val();
      var stok_ukuran = $('#ukuran_cart_' + key + ' option[value="'+ukuran+'"]').data('stok');

      if (ukuran == '') {
        Swal.fire(
          'Perhatian!',
          'Harap pilih ukuran terlebih dahulu',
          'warning'
        );

        return false;
      }

      if (jumlah == '' || jumlah <= 0) {
        Swal.fire(
          'Perhatian!',
          'Jumlah yang diinputkan harus lebih dari 0',
          'warning'
        );
        return false;
      }

      if (stok_ukuran < jumlah) {
        Swal.fire(
          'Perhatian!',
          'Jumlah yang diinputkan melebih stok yang disediakan',
          'warning'
        );
        return false;
      }

      $.ajax({
          type: 'POST',
          url: "{{ route('add-cart') }}",
          headers: {'X-CSRF-TOKEN': "{{ csrf_token() }}"},
          data: {
            "_token": "{{ csrf_token() }}",
            "produk_id": produk_id,
            "jumlah": jumlah,
            "ukuran": ukuran,
          },
          success: function (data) {
            console.log(data);
            if (data.error) {
              Swal.fire(
                'Perhatian!',
                data.message,
                'warning'
              );
            } else {
              Swal.fire(
                'Berhasil!',
                data.message,
                'success'
              );
              getCart();
              $('#exampleModal' + produk_id).modal('hide');
            }
          },
      });
    });
    </script>

  @yield('js-content')
</body>
</html>
