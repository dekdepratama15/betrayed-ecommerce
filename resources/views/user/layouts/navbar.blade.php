<header>
  <!-- Header desktop -->
  <div class="container-menu-desktop">
    <!-- Topbar -->
    <div class="wrap-menu-desktop">
      <nav class="limiter-menu-desktop container">

        <!-- Logo desktop -->
        <a href="#" class="logo">
          <img src="{{ asset('user/images/betrayerd.png') }}" alt="IMG-LOGO">
        </a>

        <!-- Menu desktop -->
        <div class="menu-desktop">
          <ul class="main-menu">
            <li class="{{ request()->is('/') ? 'active-menu' : '' }}">
              <a href="{{ route('home') }}">Beranda</a>
            </li>

            <li class="{{ request()->is('semua-produk') ? 'active-menu' : '' }}" >
              <a href="{{ route('shop.user') }}">Belanja</a>
            </li>

            <li class="{{ request()->is('blog') ? 'active-menu' : '' }}" >
                <a href="{{ route('blog') }}">Blog</a>
            </li>

            <li class="{{ request()->is('company-profile') ? 'active-menu' : '' }}" >
                <a href="{{ route('company-setting') }}">Tentang</a>
            </li>
          </ul>
        </div>

        <!-- Icon header -->
        <div class="wrap-icon-header flex-w flex-r-m">
            @if (auth()->user())
                <div class="icon-header-item cl2 hov-cl1 trans-04 p-l-22 p-r-11 icon-header-noti js-show-cart" id="cart-notif" data-notify="0">
                <i class="zmdi zmdi-shopping-cart"></i>
                </div>
            @endif

          <div class="ml-4">
            @if (auth()->user())
            <div class="dropdown">
              <button style="font-size: 30px;margin-top:-3px;/" class="dis-block icon-header-item cl2 hov-cl1 trans-04" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="zmdi zmdi-account"></i>
              </button>
              <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
                <a class="dropdown-item" href="{{route('profile.user')}}">Akun</a>
                <a class="dropdown-item" href="{{route('profile.user')}}?tab=v-pills-dashboard">Pembelian</a>
                <a class="dropdown-item" href="{{route('logout')}}" onclick="event.preventDefault();
                document.getElementById('logout-form').submit();">Logout</a>
                <form id="logout-form" action="{{ route('logout') }}" method="POST" class="d-none">
                  @csrf
                </form>
              </div>
            </div>
            @else
            <button type="button" class="btn btn-secondary btn-sm dis-block icon-header-item cl2 hov-cl1 trans-04" data-toggle="modal" data-target="#modallogin">
              Masuk
            </button>
            @endif
          </div>

        </div>
      </nav>
    </div>
  </div>

  <!-- Header Mobile -->
  <div class="wrap-header-mobile">
    <!-- Logo moblie -->
    <div class="logo-mobile">
      <a href="index.html"><img src="images/icons/logo-01.png" alt="IMG-LOGO"></a>
    </div>

    <!-- Icon header -->
    <div class="wrap-icon-header flex-w flex-r-m m-r-15">
      <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 js-show-modal-search">
        <i class="zmdi zmdi-search"></i>
      </div>

      <div class="icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti js-show-cart" data-notify="2">
        <i class="zmdi zmdi-shopping-cart"></i>
      </div>

      <a href="#" class="dis-block icon-header-item cl2 hov-cl1 trans-04 p-r-11 p-l-10 icon-header-noti" data-notify="0">
        <i class="zmdi zmdi-favorite-outline"></i>
      </a>
    </div>

    <!-- Button show menu -->
    <div class="btn-show-menu-mobile hamburger hamburger--squeeze">
      <span class="hamburger-box">
        <span class="hamburger-inner"></span>
      </span>
    </div>
  </div>


  <!-- Menu Mobile -->
  <div class="menu-mobile">

    <ul class="main-menu-m" style="margin-bottom: 0px">
        <li>
            <a href="{{ route('home') }}">Beranda</a>
        </li>

        <li>
            <a href="{{ route('shop.user') }}">Shop</a>
        </li>

        <li class="" >
            <a href="{{ route('blog') }}">Blog</a>
        </li>

        <li class="" >
            <a href="{{ route('company-setting') }}">Tentang</a>
        </li>
    </ul>
  </div>

    {{-- Modal Login --}}
    <div class="modal fade" id="modallogin" tabindex="1" role="dialog" aria-labelledby="modalloginLabel" aria-hidden="true">
      <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
          <div class="modal-header border-0 d-flex justify-content-end">
            <button type="button" class="close text-end" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
          <div class="modal-body">
            <ul class="nav nav-tabs justify-content-center" id="myTab" role="tablist">
              <li class="nav-item">
                <a class="nav-link active" id="login-tab" data-toggle="tab" href="#login" role="tab" aria-controls="login" aria-selected="true">Login</a>
              </li>
              <li class="nav-item">
                <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab" aria-controls="profile" aria-selected="false">Register</a>
              </li>
            </ul>
            <div class="tab-content" id="myTabContent">
              {{-- Form Login --}}
              <div class="tab-pane fade show active" id="login" role="tabpanel" aria-labelledby="login-tab">
                <div class="row">
                  <div class="col-lg-5 d-flex align-items-center justify-content-center">
                    <div class="">
                      <img src="{{ asset('user/images/betrayerd.png') }}" alt="" width="200px">
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <form action="{{route('login')}}" method="POST" autocomplete="off">
                      @csrf
                      <div class="my-3 text-center">
                        <div class="h4">Masuk untuk melanjutkan</div>
                      </div>
                      <div class="card my-3">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-email"></i></span>
                                  </div>
                                  <input type="email" name="email" class="form-control" placeholder="user@example.com" value="">
                                </div>
                                @error('email')
                                    <div style="margin-top: -10px">
                                        <span class="text-danger" style="font-size: 11px">{{ $message }}</span>
                                    </div>
                                @enderror
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-lock-outline"></i></span>
                                  </div>
                                  <input type="password" name="password" class="form-control" placeholder="******" value="">
                                </div>
                                @error('password')
                                    <div style="margin-top: -10px">
                                        <span class="text-danger" style="font-size: 11px">{{ $message }}</span>
                                    </div>
                                @enderror
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <button type="submit" class="btn btn-primary w-full">Login</button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
              {{-- Form Login End --}}
              {{-- Form Regrister --}}
              <div class="tab-pane fade" id="profile" role="tabpanel" aria-labelledby="profile-tab">
                <div class="row">
                  <div class="col-lg-5 d-flex align-items-center justify-content-center">
                    <div class="">
                        <img src="{{ asset('user/images/betrayerd.png') }}" alt="" width="200px">
                    </div>
                  </div>
                  <div class="col-lg-7">
                    <form action="{{route('register')}}" method="POST" autocomplete="off" id="form-register">
                      @csrf
                      <div class="my-3 text-center">
                        <div class="h4">Daftar untuk melanjutkan</div>
                      </div>
                      <div class="card my-3">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Nama Lengkap</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-account-add"></i></span>
                                  </div>
                                  <input type="text" name="nama_lengkap" class="form-control" placeholder="Nama Lengkap" required value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Username</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-account"></i></span>
                                  </div>
                                  <input type="text" name="username" class="form-control" placeholder="Username" required value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">No Telpon</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-phone"></i></span>
                                  </div>
                                  <input type="text" name="telp" class="form-control" placeholder="No Telpon" required value="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="card my-2">
                        <div class="card-body">
                          <div class="row">
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Email</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-email"></i></span>
                                  </div>
                                  <input type="email" name="email" class="form-control" placeholder="user@example.com" required value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Password</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-lock-outline"></i></span>
                                  </div>
                                  <input type="password" name="password" class="form-control" placeholder="******" required value="">
                                </div>
                              </div>
                            </div>
                            <div class="col-lg-12">
                              <div class="form-group">
                                <label for="exampleInputEmail1">Konfirmasi Password</label>
                                <div class="input-group mb-3">
                                  <div class="input-group-prepend">
                                    <span class="input-group-text" id="basic-addon1"><i class="zmdi zmdi-lock-outline"></i></span>
                                  </div>
                                  <input type="password" name="password_confirmation" class="form-control" placeholder="******" required value="">
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-lg-12">
                          <button type="submit" class="btn btn-primary w-full d-flex align-items-center justify-content-center"><span id="login-text">Daftar</span> <div class="spinner-border spinner-border-sm text-light ml-2 d-none" role="status" id="loading-icon" ></div></button>
                        </div>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
</header>

<div class="wrap-header-cart js-panel-cart">
    <div class="s-full js-hide-cart"></div>

    <div class="header-cart flex-col-l p-l-65 p-r-25">
      <div class="header-cart-title flex-w flex-sb-m p-b-8">
        <span class="mtext-103 cl2">
            Keranjang kamu
        </span>

        <div class="fs-35 lh-10 cl2 p-lr-5 pointer hov-cl1 trans-04 js-hide-cart">
          <i class="zmdi zmdi-close"></i>
        </div>
      </div>

      <div class="header-cart-content flex-w js-pscroll">
        <div id="body-cart">

        </div>
        <div class="w-full">
            <div class="header-cart-total w-full p-tb-40" id="total_produk">
                Total: $75.00
            </div>

            <div class="header-cart-buttons flex-w w-full justify-content-center">
                <a href="{{ route('detail-cart') }}" class="flex-c-m stext-101 cl0 size-107 bg3 bor2 hov-btn3 p-lr-15 trans-04 m-r-8 m-b-10" style="text-decoration: none">
                Lihat Keranjang
                </a>
            </div>
        </div>
      </div>
    </div>
  </div>

  <style>
    .floating-whatsapp {
        position: fixed;
        bottom: 20px;
        right: 20px;
        z-index: 1000;
    }

    .floating-whatsapp img {
        width: 80px;
        height: auto;
        border-radius: 50%;
        box-shadow: 0px 2px 10px rgba(0, 0, 0, 0.2); /* Efek bayangan (shadow) */
    }
  </style>

    <div class="floating-whatsapp">
        <a href="https://wa.me/622081236419284" target="_blank">
            <img src="{{ asset('wa_icon.png') }}" alt="WhatsApp Logo">
        </a>
    </div>

