@extends('admin.layouts.app')

@section('title')
    Voucher
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Tambah Data Voucher</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('voucher.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('voucher.store') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @csrf
      <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12">
                <div>
                    <label>Nama Voucher</label>
                    <input type="text" name="nama" class="input w-full border mt-2" placeholder="Masukkan nama voucher" value="" required >
                </div>
            </div>
            <div class="col-span-6">
                <div>
                    <label>Tipe Voucher</label>
                    <select type="text" name="type" class="input w-full border mt-2" value="" required id="select-type" >
                        <option value="" selected disabled>Pilih Tipe Voucher</option>
                        <option value="persentase">Persentase</option>
                        <option value="nominal">Nominal</option>
                    </select>
                </div>
            </div>
            <div class="col-span-6" id="persentaseBody">
                <div>
                    <label>Persentase</label>
                    <input type="text" name="persentase" class="input w-full border mt-2" placeholder="Masukkan presentase voucher" id="persentaseInput" value="" >
                </div>
            </div>
            <div class="col-span-6 hidden" id="nominalBody">
                <div>
                    <label>Nominal</label>
                    <input type="text" name="nominal" class="input w-full border mt-2" placeholder="Masukkan nominal voucher" id="nominalInput" value="" >
                </div>
            </div>
            <div class="col-span-8">
                <div>
                    <label>Tanggal hangus voucher</label>
                    <input type="date" name="berlaku_sampai" class="input w-full border mt-2" placeholder="Masukkan tanggal berlaku voucher" value="" required>
                </div>
            </div>
            <div class="col-span-4">
                <div>
                    <label>Jumlah</label>
                    <input type="text" name="jumlah" class="input w-full border mt-2" placeholder="Masukkan jumlah voucher" value="" required >
                </div>
            </div>
            <div class="col-span-12">
                <div>
                    <label for="">Gambar Voucher</label>
                    <input type="file" name="gambar_voucher" class="input w-full border mt-2 bg-white" placeholder="Masukkan jumlah voucher" value="" required >
                </div>
            </div>
            <div class="col-span-12">
                <div>
                    <label for="">Deskripsi</label>
                    <textarea type="text" name="deskripsi" class="input w-full border mt-2" rows="5" placeholder="Masukkan jumlah voucher" value="" required ></textarea>
                </div>
            </div>
            <div class="col-span-12 flex justify-end" >
                <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
            </div>
      </div>
    </form>
  </div>
@endsection

@section('js-content')
  <script>
    $(document).ready(function(){
        $('#select-type').change(function(){
            var selectOption = $(this).val();
            if (selectOption == 'persentase') {
                $('#persentaseBody').removeClass('hidden');
                $('#nominalBody').addClass('hidden');
            } else if (selectOption == 'nominal') {
                $('#persentaseBody').addClass('hidden');
                $('#nominalBody').removeClass('hidden');
            }
        })
    })
  </script>
@endsection
