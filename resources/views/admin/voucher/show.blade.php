@extends('admin.layouts.app')

@section('title')
    Voucher
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Detail Data Voucher</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('voucher.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('voucher.update',$voucher->id) }}" method="POST" autocomplete="off">
      @method('PUT')
      @csrf
      <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12">
                <div>
                    <label>Nama Voucher</label>
                    <input type="text" name="nama" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan nama voucher" value="{{ $voucher->nama }}" required readonly >
                </div>
            </div>
            <div class="col-span-6">
                <div>
                    <label>Tipe Voucher</label>
                    <input type="text" name="type" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan tiper voucher" value="{{ $voucher->type }}" required  readonly >
                </div>
            </div>
            <div class="col-span-6" id="persentaseBody">
                <div>
                    <label>Persentase</label>
                    <input type="text" name="persentase" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan persentase voucher" value="{{ $voucher->persentase }}" required readonly >
                </div>
            </div>
            <div class="col-span-6 hidden" id="nominalBody">
                <div>
                    <label>Nominal</label>
                    <input type="text" name="nominal" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan nominal voucher" value="{{ $voucher->nominal }}" required readonly >
                </div>
            </div>
            <div class="col-span-8">
                <div>
                    <label>Tanggal hangus voucher</label>
                    <input type="date" name="berlaku_sampai" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan tanggal berlaku voucher" value="{{ date('Y-m-d',strtotime($voucher->berlaku_sampai)) }}" required readonly >
                </div>
            </div>
            <div class="col-span-4">
                <div>
                    <label>Jumlah</label>
                    <input type="text" name="jumlah" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan jumlah voucher" value="{{ $voucher->jumlah }}" required readonly >
                </div>
            </div>
            <div class="col-span-12">
                <div>
                    <label for="">Deskripsi</label>
                    <textarea type="text" name="deskripsi" class="input w-full border mt-2" rows="5" placeholder="Masukkan jumlah voucher" value="" required >{{ $voucher->deskripsi }}</textarea>
                </div>
            </div>
            <div class="col-span-12">
                <div>
                    <label for="">Gambar Voucher</label>
                    <img src="{{ asset('upload/'.$voucher->gambar_voucher) }}" alt="" width="200px" height="200px">
                </div>
            </div>
      </div>
    </form>
  </div>
@endsection

@section('js-content')
  <script>
    $(document).ready(function(){
        var dataVoucher = {!! json_encode($voucher) !!};
        var selectedType = dataVoucher.type;
        if (selectedType == 'persentase') {
            $('#persentaseBody').removeClass('hidden');
            $('#nominalBody').addClass('hidden');
        }else{
            $('#persentaseBody').addClass('hidden');
            $('#nominalBody').removeClass('hidden');
        }

        $('#select-type').change(function(){
            var selectOption = $(this).val();
            if (selectOption == 'persentase') {
                $('#persentaseBody').removeClass('hidden');
                $('#nominalBody').addClass('hidden');
            } else if (selectOption == 'nominal') {
                $('#persentaseBody').addClass('hidden');
                $('#nominalBody').removeClass('hidden');
            }
        })
    })
  </script>
@endsection
