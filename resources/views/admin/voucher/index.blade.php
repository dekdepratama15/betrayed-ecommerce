@extends('admin.layouts.app')

@section('title')
    Voucher
@endsection

@section('content')
<div class="col-span-12 my-6">
  <div class="flex justify-content-between justify-between">
      <h2 class="text-lg font-medium truncate">Data List Voucher</h2>
  </div>
  <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center mt-2">
      <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
          <div class="w-56 relative text-gray-700">
                <form action="" method="get" autocomplete="off">
                    <input type="text" class="input w-56 box pr-10 placeholder-theme-13" name="search" value="{{ $search }}" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                </form>
          </div>
      </div>
      <a href="{{route('voucher.create')}}" class="button w-36 mb-2 mr-2 mt-2 flex items-center justify-center bg-theme-1 text-white"> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Tambah Data </a>
  </div>
</div>
<!-- BEGIN: Data List -->
<div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
  <table class="table table-report -mt-2">
      <thead>
          <tr>
              <th class="whitespace-no-wrap">No</th>
              <th class="whitespace-no-wrap">Nama Voucher</th>
              <th class="whitespace-no-wrap">Tipe Vourcher</th>
              <th class="whitespace-no-wrap">Presentase / Nominal</th>
              <th class="whitespace-no-wrap">Jumlah</th>
              <th class="text-center whitespace-no-wrap">Aksi</th>
          </tr>
      </thead>
      @php
         $index = 0
      @endphp
      <tbody>
          @if(count($vouchers) > 0)
            @foreach ($vouchers as $voucher)
            <tr class="intro-x">
                <td class="w-10">{{ ++$index }}</td>
                <td class="w-20">{{ $voucher->nama }}</td>
                <td class="w-20">{{ $voucher->type == 'persentase' ? 'Persentase' : 'Nominal' }}</td>
                @if ($voucher->type == 'persentase')
                <td class="w-20">{{ $voucher->persentase }} %</td>
                @else
                <td class="w-20">{{ $voucher->nominal }}</td>
                @endif
                <td class="w-20">{{ $voucher->jumlah }}</td>
                <td class="table-report__action w-10">
                    <div class="flex justify-center items-center">
                        <a class="flex items-center mr-3" href="{{ route('voucher.edit', $voucher->id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                        <a class="flex items-center mr-3 text-theme-3" href="{{ route('voucher.show',$voucher->id) }}"> <i data-feather="alert-circle" class="w-4 h-4 mr-1"></i> Detail </a>
                        <button type="button" class="flex items-center text-theme-6 delete-data" data-redirect="{{route('voucher.destroy',$voucher->id)}}" data-id="{{ $voucher->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Hapus </button>
                    </div>
                </td>
            </tr>
            @endforeach
          @else
            <tr>
                <td class="intro-x text-center" colspan="6">Tidak terdapat data Voucher</td>
            </tr>
            @endif
      </tbody>
  </table>
  <div class="row">
    <div class="col-12 mt-2 mt-md-4">
        <ul class="pagination pagination_style1 justify-content-center">
           {{ $vouchers->links() }}
        </ul>
    </div>
</div>
</div>
<!-- END: Data List -->
@endsection
