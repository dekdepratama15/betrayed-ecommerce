@php
$routes = [
  // Dashboard
  [
    'name' => 'Dashboard',
    'route' => 'dashboard.index',
    'active' => 'admin/dashboard',
    'icon' => 'home'
  ]
  ,
  // User
  [
    'name' => 'Customer',
    'route' => 'customer.index',
    'active' => 'admin/customer',
    'icon' => 'users',
  ]
  ,
  // Produk
  [
    'name' => 'Produk',
    'icon' => 'box',
    'route' => '',
    'active' => 'admin/produk*',
    'childs' => [
      [
        'name' => 'Produk',
        'route' => 'product.index',
        'sub-active' => 'admin/produk/product*',
        'icons' => 'circle'
      ]
      ,
      [
        'name' => 'Kategori',
        'route' => 'kategori.index',
        'sub-active' => 'admin/produk/kategori*',
        'icons' => 'circle'
      ]
    ]
  ],

  // Jasa Pengiriman
  [
      'name' => 'Jasa Pengiriman',
      'route' => 'jasa-pengiriman.index',
      'active' => 'admin/jasa-pengiriman',
      'icon' => 'send',
  ],

  // Blog
  [
      'name' => 'Blog',
      'route' => 'blog.index',
      'active' => 'admin/blog',
      'icon' => 'book',
  ],

  // Order
  [
      'name' => 'Pesanan',
      'route' => 'order.index',
      'active' => 'admin/order',
      'icon' => 'shopping-cart',
],

[
    'name' => 'Voucher',
    'route' => 'voucher.index',
    'active' => 'admin/voucher',
    'icon' => 'credit-card',
],
// Setting Company
[
    'name' => 'Setting Company',
    'route' => 'company.edit',
    'active' => 'admin/company',
    'icon' => 'sliders',
    'id' => 1,
],
    //
  //
]
@endphp

<!-- BEGIN: Mobile Menu -->
<div class="mobile-menu md:hidden">
  <div class="mobile-menu-bar">
      <a href="" class="flex mr-auto">
          <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{asset('user/images/betrayerd.png')}}">
      </a>
      <a href="javascript:;" id="mobile-menu-toggler"> <i data-feather="bar-chart-2" class="w-8 h-8 text-white transform -rotate-90"></i> </a>
  </div>
  <ul class="border-t border-theme-24 py-5 hidden">
    @foreach ($routes as $route)
        @php
            $route_id = '';
            if (isset($route['id']) && !empty($route['id'])) {
                $route_id = $route['id'];
            }
        @endphp
      <li>
          <a href="{{ isset($route['childs']) ? 'javascript:;' : (!empty($route_id) ? route($route['route'], $route_id) : route($route['route'])) }}"  class="menu {{ request()->is($route['active']) ? 'menu--active' : '' }} @if (isset($route['childs'])) {{ request()->is($route['active']) ? 'menu--open' : '' }} @endif ">
              <div class="menu__icon"> <i data-feather="{{$route['icon']}}"></i> </div>
              <div class="menu__title"> {{$route['name']}} @if(isset($route['childs'])) <i data-feather="chevron-down" class="menu__sub-icon"></i> @endif</div>
          </a>

          @if(isset($route['childs']))
            <ul class="class="{{ request()->is($route['active']) ? 'menu__sub-open' : '' }}">
                @foreach ($route['childs'] as $child)
                  <li>
                      <a href="{{route($child['route'])}}" class="menu {{ request()->is($child['sub-active']) ? 'menu--active' : '' }}">
                          <div class="menu__icon"> <i data-feather="{{$child['icons']}}"></i> </div>
                          <div class="menu__title">{{$child['name']}}</div>
                      </a>
                  </li>
                @endforeach
            </ul>
          @endif
      </li>
    @endforeach
  </ul>
</div>
<!-- END: Mobile Menu -->

<div class="flex">
  <nav class="side-nav">
    <a href="" class="intro-x flex items-center pl-5 pt-4">
        <img alt="Midone Tailwind HTML Admin Template" class="w-6" src="{{asset('user/images/betrayerd.png')}}">
        <span class="hidden xl:block text-white text-lg ml-3"> Admin<span class="font-medium"> Betrayed</span> </span>
    </a>
    <div class="side-nav__devider my-6"></div>
    <ul>
      @foreach ($routes as $route)
        @php
            $route_id = '';
            if (isset($route['id']) && !empty($route['id'])) {
                $route_id = $route['id'];
            }
        @endphp
        <li>
            <a href="{{ isset($route['childs']) ? 'javascript:;' : (!empty($route_id) ? route($route['route'], $route_id) : route($route['route']))}}"  class="side-menu  {{ request()->is($route['active']) ? 'side-menu--active' : '' }} @if (isset($route['childs'])) {{ request()->is($route['active']) ? 'side-menu--open' : '' }} @endif ">
                <div class="side-menu__icon"> <i data-feather="{{$route['icon']}}"></i> </div>
                <div class="side-menu__title"> {{$route['name']}} @if(isset($route['childs'])) <i data-feather="chevron-down" class="side-menu__sub-icon"></i> @endif</div>
            </a>
            @if(isset($route['childs']))
              <ul class="{{ request()->is($route['active']) ? 'side-menu__sub-open' : '' }}">
                  @foreach ($route['childs'] as $child)
                    <li>
                        <a href="{{route($child['route'])}}" class="side-menu {{ request()->is($child['sub-active']) ? 'side-menu--active' : '' }}">
                            <div class="side-menu__icon"> <i data-feather="{{$child['icons']}}"></i> </div>
                            <div class="side-menu__title">{{$child['name']}}</div>
                        </a>
                    </li>
                  @endforeach
              </ul>
            @endif
        </li>
      @endforeach
    </ul>
  </nav>
</div>
