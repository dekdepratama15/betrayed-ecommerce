<!DOCTYPE html>
<html lang="en">
<head>
  <meta charset="utf-8">
        <link href="dist/images/logo.svg" rel="shortcut icon">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="description" content="Midone admin is super flexible, powerful, clean & modern responsive tailwind admin template with unlimited possibilities.">
        <meta name="keywords" content="admin template, Midone admin template, dashboard template, flat admin template, responsive admin template, web app">
        <meta name="author" content="LEFT4CODE">
        <title>Betrayed</title>
        <!-- BEGIN: CSS Assets-->
        <link rel="stylesheet" href="{{asset('admin/dist/css/app.css')}}" />
        <link rel="stylesheet" type="text/css" href="{{asset('user/fonts/font-awesome-4.7.0/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.css" integrity="sha512-nNlU0WK2QfKsuEmdcTwkeh+lhGs6uyOxuUs+n+0oXSYDok5qy0EI0lt01ZynHq6+p/tbgpZ7P+yUb+r71wqdXg==" crossorigin="anonymous" referrerpolicy="no-referrer" />
        <!-- END: CSS Assets-->
</head>
<body @guest class="login" @else class="app" @endguest >
  @guest
      @yield('content')
  @else
  <div class="flex">
    @include('admin.layouts.navbar')
    <div class="content">
      @include('admin.layouts.topbar')
      @yield('content')
    </div>
  </div>
  @endguest


  <!-- BEGIN: JS Assets-->
  <script src="https://developers.google.com/maps/documentation/javascript/examples/markerclusterer/markerclusterer.js"></script>
  <script src="https://maps.googleapis.com/maps/api/js?key=["your-google-map-api"]&libraries=places"></script>
  <script src="{{asset('admin/dist/js/app.js')}}"></script>
  <script src="https://unpkg.com/@yaireo/tagify"></script>
  <script src="https://unpkg.com/@yaireo/tagify/dist/tagify.polyfills.min.js"></script>
  {{-- <script src="{{asset('admin/dist/js/jquery.js')}}"></script> --}}
  <script src="https://code.jquery.com/jquery-3.7.1.min.js" integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
  <script src="https://cdn.jsdelivr.net/npm/sweetalert2@11"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-maskmoney/3.0.2/jquery.maskMoney.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/fancybox/3.5.7/jquery.fancybox.min.js?v2" integrity="sha512-uURl+ZXMBrF4AwGaWmEetzrd+J5/8NRkWAvJx5sbPSSuOb0bZLqf+tOzniObO00BjHa/dD7gub9oCGMLPQHtQA==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>


  <!-- END: JS Assets-->
  <script>
    $(document).ready(function(){
      $('.fancybox').fancybox();
    })
  </script>

  <script>
    $(document).ready(function () {
        $('.delete-data').click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            var token = $(this).data("token");
            var redirect = $(this).data("redirect");
            Swal.fire({
                title: 'Yakin?',
                text: "Data akan dihapus!",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Hapus!',
                cancelButtonText: 'Batal'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "delete",
                        url: redirect,
                        data: {
                            "id": id,
                            "_method": 'DELETE',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Berhasil!',
                                response.message,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
    });

  </script>


  @if(session('success'))
  <script>
  $(document).ready(function() {
      Swal.fire(
          'Berhasil!',
          '{{session("success")}}',
          'success'
      );
  });
  </script>
  @endif

  @if(session('warning'))
  <script>
  $(document).ready(function() {
      Swal.fire(
          'Gagal!',
          '{{session("warning")}}',
          'warning'
      );
  });
  </script>
  @endif


{{-- Format Rupiah --}}
<script>
$(document).ready(function(){
    $('[data-format_rupiah="formatRupiah"]').maskMoney({thousands:'.', decimal:',', precision:0});
});

function formatRupiah(str) {
    return str.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",");
}
</script>
{{-- Format Rupiah --}}

@yield('js-content')
</body>
</html>
