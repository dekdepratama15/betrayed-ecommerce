@extends('admin.layouts.app')

@section('title')
    Pesanan
@endsection

@section('content')
<div class="col-span-12 my-6">
    <div class="flex justify-content-between justify-between">
        <h2 class="text-lg font-medium truncate">Data List Pesanan</h2>
    </div>
    <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center mt-2">
        <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
            <div class="w-56 relative text-gray-700">
                <form action="" method="get" class="position-relative">
                    <input type="text" value="{{ $search }}" name="search" autocomplete="off" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                    <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                </form>
            </div>
        </div>
        <div class="flex gap-5">
            <div>
                <div>
                    <select name="select_year" id="select_year" class="input w-56 border mt-2 intro-y">
                        <option value="all" selected>Semua Tahun</option>
                        @php
                        $currentYear = date("Y");
                        $startYear = $currentYear - 5;
                        $endYear = $currentYear + 5;
                        for($year = $startYear; $year <= $endYear; $year++) {
                            echo "<option value=\"$year\" ".($year == $select_year ? 'selected' : '').">$year</option>";
                        }
                        @endphp
                    </select>
                </div>
            </div>
            <div class="">
                <div>
                    @php
                        $bulan = [
                        'Januari',
                        'Februari',
                        'Maret',
                        'April',
                        'Mei',
                        'Juni',
                        'Juli',
                        'Agustus',
                        'September',
                        'Oktober',
                        'November',
                        'Desember'
                        ];
                    @endphp
                    <select name="" id="select_month" class="input w-56 border mt-2 intro-y">
                        <option value="all" selected>Semua Bulan</option>
                        @foreach ($bulan as $key => $value)
                            <option value="{{ $key + 1 }}" {{($key+1) == $select_month ? 'selected' : '' }}>{{ $value }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="row button-group mt-2 intro-y" style="text-align: right;margin-bottom: 12px">
                <div class="col-lg-12">
                    <form action="{{ route('orderExcel') }}" method="get" class="position-relative">
                        <input type="hidden" class="form-control w-25" value="{{ $search }}" name="search" autocomplete="off" placeholder="Search...">
                        <input type="hidden" class="form-control w-25" value="{{ $select_month }}" name="month" autocomplete="off" placeholder="Search...">
                        <input type="hidden" class="form-control w-25" value="{{ $select_year }}" name="year" autocomplete="off" placeholder="Search...">
                        <button type="submit"
                            class="button w-36 mb-2 flex items-center justify-center bg-green-700 text-white"
                            >
                            Export Excel
                        </button>
                    </form>
                </div>
            </div>
        </div>
        {{-- <a href="{{route('blog.create')}}" class="button w-36 mb-2 mr-2 mt-2 flex items-center justify-center bg-theme-1 text-white"> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Tambah Data </a> --}}
    </div>

  </div>
  <!-- BEGIN: Data List -->
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <table class="table table-report -mt-2">
        <thead>
            <tr>
                <th class="whitespace-no-wrap">No</th>
                <th class="whitespace-no-wrap">Invoice</th>
                <th class="whitespace-no-wrap">Customer</th>
                <th class="whitespace-no-wrap">Alamat</th>
                <th class="whitespace-no-wrap">Total</th>
                <th class="whitespace-no-wrap">Status</th>
                <th class="text-center whitespace-no-wrap">Aksi</th>
            </tr>
        </thead>
        @php
            $index = 0
        @endphp
        <tbody>
            @if (count($data) > 0)
            @foreach ($data as $orders)
            <tr class="intro-x">
                <td class="w-10">{{ ++$index }}</td>
                <td class="w-64">{{ ucfirst($orders->invoice) }}</td>
                <td class="w-64">{{ ucfirst($orders->customers->nama_lengkap) }}</td>
                <td class="w-64">{{ ucfirst($orders->alamat) }}</td>
                <td class="w-64">Rp {{ number_format($orders->total,0,',','.') }}</td>
                <td class="w-64">
                    @if ($orders->status == 'pending')
                        <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                            Konfirmasi Pesanan
                        </span>
                    @elseif($orders->status == 'menunggu_pembayaran')
                        <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                            Menunggu Pembayaran
                        </span>
                    @elseif($orders->status == 'menunggu_persetujuan')
                        <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                            Konfirmasi Pembayaran
                        </span>
                    @elseif($orders->status == 'terbayar')
                        <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                            Sudah terbayar
                        </span>
                    @elseif($orders->status == 'pengiriman')
                        <span class="bg-blue-100 text-blue-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-blue-900 dark:text-blue-300">
                            Sedang Pengiriman
                        </span>
                    @elseif($orders->status == 'diterima')
                        <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                            Pesanan Diterima
                        </span>
                    @elseif($orders->status == 'dibatalkan')
                        <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                            Dibatalkan
                        </span>
                    @elseif($orders->status == 'ditolak')
                    <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                        Ditolak
                    </span>
                    @endif
                </td>
                <td class="table-report__action w-16">
                    <div class="flex justify-center items-center">
                        @if ($orders->status == 'pending')
                            <button class="flex items-center mr-3 konfirmasi_pesanan" data-redirect="{{ route('confirmOrder',$orders->id) }}" data-id="{{ $orders->id }}" data-token="{{ csrf_token() }}" > <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Konfirmasi Pesanan </button>
                        @elseif($orders->status == 'menunggu_persetujuan')
                            <a class="flex items-center mr-3" href="{{ route('pageConfirmPayment',$orders->id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Konfirmasi Pembayaran </a>
                        @elseif($orders->status == 'terbayar')
                            <a class="flex items-center mr-3" href="{{ route('pageConfirmPengiriman',$orders->id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Kirim Pesanan </a>
                        @endif
                        <a class="flex items-center mr-3 text-theme-3" href="{{ route('order.show',$orders->id) }}"> <i data-feather="alert-circle" class="w-4 h-4 mr-1"></i> Detail </a>

                    </div>
                </td>
            </tr>
            @endforeach
            @else
            <tr>
                <td class="intro-x text-center" colspan="7">Tidak terdapat data Pesanan</td>
            </tr>
            @endif
        </tbody>
    </table>
    <div class="row">
        <div class="col-12 mt-2 mt-md-4">
            <ul class="pagination pagination_style1 justify-content-center">
               {{ $data->links() }}
            </ul>
        </div>
    </div>
  </div>
  <!-- END: Data List -->

@endsection

@section('js-content')
<script>
    $(document).ready(function () {
        $('.konfirmasi_pesanan').click(function (e) {
            e.preventDefault();
            var id = $(this).data("id");
            var token = $(this).data("token");
            var redirect = $(this).data("redirect");
            Swal.fire({
                title: 'Yakin?',
                text: "Menyetujui Pemesanan",
                icon: 'warning',
                showCancelButton: true,
                confirmButtonColor: '#0275d8',
                cancelButtonColor: '#d33',
                confirmButtonText: 'Ya, Setujui!',
                cancelButtonText: 'Batal'
            })
            .then((result) => {
                if (result.isConfirmed) {
                    $.ajax({
                        type: "post",
                        url: redirect,
                        data: {
                            "id": id,
                            "_method": 'POST',
                            "_token": token,
                        },
                        success: function (response) {
                            Swal.fire(
                                'Berhasil!',
                                response.message,
                                'success'
                            )
                            .then((result) => {
                                location.reload();
                            });

                        }
                    });
                }
            })
        });
    });

  </script>

<script>
    $(document).ready(function(){
        $('#select_month, #select_year').on('change',function(e){
            var month = $('#select_month').val();
            var year = $('#select_year').val();
            window.location.href = "{{ route('order.index') }}?month=" + month + "&year=" + year;
        });
    });

  </script>
@endsection
