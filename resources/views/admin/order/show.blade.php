@extends('admin.layouts.app')

@section('title')
    Pesanan
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Detail Pesanan</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('order.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-12">
              <div>
                  <label>Invoice</label>
                  <input type="text" name="invoice" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Invoice" value="{{ ucfirst($data->invoice) }}" >
              </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>No Resi</label>
                <input type="text" name="no_resi" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan No Resi" value="{{ $data->no_resi }}" >
            </div>
        </div>
          <div class="col-span-12">
            <div>
                <label>Customer</label>
                <input type="text" name="customer" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Customer" value="{{ ucfirst($data->customers->nama_lengkap) }}" >
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Alamat</label>
                <textarea type="text" rows="5" readonly name="alamat" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Alamat" value="" >{{ ucfirst($data->alamat) }}</textarea>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Kordinat Alamat</label>
                <div style="width: 100px">
                    <a type="button" href="https://www.google.com/maps?q={{ $data->lat.','.$data->long }}" class="button mb-2 flex bg-theme-1 text-white" target="_blank">Google Map</a>
                </div>
            </div>
          </div>
          <div class="col-span-4">
            <div>
                <label>Nama Bank</label>
                <input type="text" name="nama_bank" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Nama Bank" value="{{ ucfirst($data->nama_bank) }}" >
            </div>
          </div>
          <div class="col-span-4">
            <div>
                <label>No Bank</label>
                <input type="text" name="no_bank" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan No Bank" value="{{ ucfirst($data->no_bank) }}" >
            </div>
          </div>
          <div class="col-span-4">
            <div>
                <label>Pemilik Bank</label>
                <input type="text" name="pemilik_bank" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan No Bank" value="{{ ucfirst($data->pemilik_bank) }}" >
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Status</label>
                <input type="text" name="status" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan status" value="{{ ucfirst($data->status) }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Voucher</label>
                <input type="text" name="voucher" readonly class="input w-full border mt-2 bg-gray-300" placeholder="" value="{{ $data->voucher }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Tipe Voucher</label>
                <input type="text" name="tipe_voucher" readonly class="input w-full border mt-2 bg-gray-300" placeholder="" value="{{ ucfirst($data->type_voucher) }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Discount</label>
                <input type="text" name="discount" readonly class="input w-full border mt-2 bg-gray-300" placeholder="" value="{{ $data->discount }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Nominal Discount</label>
                <input type="text" name="nominal_discount" readonly class="input w-full border mt-2 bg-gray-300" placeholder="" value="{{ ucfirst($data->nominal_discount) }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Total Sebelum Discount</label>
                <input type="text" name="total_sebelum_discount" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Total Sebelum Discount" value="Rp {{ number_format($data->total_sebelum_discount,0,',','.') }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Total</label>
                <input type="text" name="total" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Total" value="Rp {{ number_format($data->total,0,',','.') }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Kurir Pengiriman</label>
                <input type="text" name="shipping_courier" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Kurir Pengiriman" value="{{ ucfirst($data->shipping_courier) }}" >
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Harga Pengiriman</label>
                <input type="text" name="shipping_price" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Harga Pengiriman" value="Rp {{ number_format($data->shipping_price,0,',','.') }}" >
            </div>
          </div>
      </div>
  </div>
@endsection
