@extends('admin.layouts.app')

@section('title')
    Customer
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Ubah Data Customer</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('customer.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('customer.update',$user->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @method('PUT')
      @csrf
      <div class="p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
        <div class="grid grid-cols-12 gap-5">
            <div class="col-span-6">
                <div>
                    <label>Nama Lengkap</label>
                    <input type="text" name="nama_lengkap" class="input w-full border mt-2" placeholder="Masukkan nama lengkap" value="{{$user->customers->nama_lengkap}}" >
                </div>
            </div>
            <div class="col-span-6">
              <div>
                  <label>Username</label>
                  <input type="text" name="username" class="input w-full border mt-2" placeholder="Masukkan username" value="{{$user->username}}" >
              </div>
            </div>
            <div class="col-span-6">
              <div>
                  <label>No Telepon</label>
                  <input type="text" name="telp" class="input w-full border mt-2" placeholder="Masukkan no telpon" value="{{$user->customers->telp}}" >
              </div>
            </div>
            <div class="col-span-6">
              <div>
                  <label>Tempat Lahir</label>
                  <input type="text" name="tempat_lahir" class="input w-full border mt-2" placeholder="Masukkan tempat lahir" value="{{$user->customers->tempat_lahir}}" >
              </div>
            </div>
            <div class="col-span-12">
              <div>
                  <label>Tanggal Lahir</label>
                  <input type="date" name="tanggal_lahir" class="input w-full border mt-2" placeholder="Masukkan nama kategori" value="{{date('Y-m-d', strtotime($user->customers->tanggal_lahir))}}" >
              </div>
            </div>
            <div class="col-span-12">
              <div>
                  <label>Gambar</label>
                  <input type="file" name="img_profile" class="input w-full border mt-2 bg-white" placeholder="Masukkan nama kategori" value="" >
                  <div class="mt-4">
                    <a class="input-group-text button fancybox bg-theme-1 text-white" id="basic-addon2" data-caption="{{$user->username}}" href="{{asset('upload/'.$user->customers->img_profile)}}">Lihat Gambar</a>
                  </div>
              </div>
            </div>
        </div>
      </div>
      <div class="p-6 bg-white border border-gray-200 rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 my-3">
        <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12">
                <div>
                    <label>Email</label>
                    <input type="text" name="email" class="input w-full border mt-2" placeholder="Masukkan nama kategori" value="{{$user->email}}" >
                </div>
            </div>
            <div class="col-span-6">
              <div>
                  <label>Password</label>
                  <input type="password" name="password" class="input w-full border mt-2" placeholder="******" value="" >
              </div>
            </div>
            <div class="col-span-6">
              <div>
                  <label>Konfirmasi Password</label>
                  <input type="password" name="password_confirmation" class="input w-full border mt-2" placeholder="******" value="" >
              </div>
            </div>
        </div>
      </div>
      <div class="col-span-12 flex justify-end" >
        <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
      </div>
    </form>
  </div>
@endsection
