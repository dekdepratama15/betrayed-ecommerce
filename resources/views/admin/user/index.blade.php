@extends('admin.layouts.app')

@section('title')
    Customer
@endsection

@section('content')
    <div class="col-span-12 my-6">
        <div class="flex justify-content-between justify-between">
            <h2 class="text-lg font-medium truncate">Data List Customer</h2>
        </div>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center mt-2">
            <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
                <div class="w-56 relative text-gray-700">
                    <form action="" method="get" class="position-relative">
                        <input type="text" value="{{ $search }}" name="search" autocomplete="off" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                        <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i>
                    </form>
                </div>
            </div>
        </div>
    </div>
    <!-- BEGIN: Data List -->
    <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
        <table class="table table-report -mt-2">
            <thead>
                <tr>
                    <th class="whitespace-no-wrap">No</th>
                    <th class="whitespace-no-wrap">Akun</th>
                    <th class="text-center whitespace-no-wrap">Aksi</th>
                </tr>
            </thead>
            <tbody>
                @if (count($users) > 0)
                    @foreach ($users as $user)
                    <tr class="intro-x">
                        <td class="w-10">{{$loop->iteration}}</td>
                        <td>
                            <a href="" class="font-medium whitespace-no-wrap">{{$user->customers->nama_lengkap}}</a>
                            <div class="text-gray-600 text-xs whitespace-no-wrap">{{$user->email}}</div>
                        </td>
                        <td class="table-report__action w-56">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3" href="{{route('customer.edit',$user->id)}}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                                <button type="button" class="flex items-center text-theme-6 delete-data" data-redirect="{{route('customer.destroy',$user->id)}}" data-id="{{ $user->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Hapus </button>
                            </div>
                        </td>
                    </tr>
                    @endforeach
                @else
                    <tr>
                        <td class="intro-x text-center" colspan="5">Tidak terdapat data Customer</td>
                    </tr>
                @endif

            </tbody>
        </table>
        <div class="row">
            <div class="col-12 mt-2 mt-md-4">
                <ul class="pagination pagination_style1 justify-content-center">
                   {{ $users->links() }}
                </ul>
            </div>
        </div>
    </div>
    <!-- END: Data List -->
@endsection
