@extends('admin.layouts.app')

@section('title')
    Kategori
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Edit Data Kategori</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('kategori.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('kategori.update',$kategori->id) }}" method="POST" autocomplete="off">
      @method('PUT')
      @csrf
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-12">
              <div>
                  <label>Nama Kategori</label>
                  <input type="text" name="name" class="input w-full border mt-2" placeholder="Masukkan nama kategori" value="{{ ucfirst($kategori->name) }}" >
              </div>
          </div>
          <div class="col-span-12 flex justify-end" >
            <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
          </div>
      </div>
    </form>
  </div>
@endsection
