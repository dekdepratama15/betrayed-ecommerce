@extends('admin.layouts.app')

@section('title')
    Dashboard
@endsection

@section('content')
  <div class="grid grid-cols-12 gap-6">
    <h2 class="text-lg font-medium truncate mt-8">Dashboard</h2>
    <div class="col-span-12">
        <div class="grid grid-cols-12 gap-6 mt-5">
            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                <div class="report-box zoom-in">
                    <div class="box p-5">
                        <div class="flex">
                            <i data-feather="shopping-cart" class="report-box__icon text-theme-10"></i>
                        </div>
                        <div class="text-3xl font-bold leading-8 mt-6">{{ $produk }}</div>
                        <div class="text-base text-gray-600 mt-1">Total Produk</div>
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6 xl:col-span-3 intro-y">
                <div class="report-box zoom-in">
                    <div class="box p-5">
                        <div class="flex">
                            <i data-feather="user" class="report-box__icon text-theme-9"></i>
                        </div>
                        <div class="text-3xl font-bold leading-8 mt-6">{{ $customer }}</div>
                        <div class="text-base text-gray-600 mt-1">Total Customer</div>
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
                <div class="report-box zoom-in">
                    <div class="box p-5">
                        <div class="flex">
                            <i data-feather="clock" class="report-box__icon text-theme-11"></i>
                        </div>
                        <div class="text-3xl font-bold leading-8 mt-6" id="count_belum_selesai">0</div>
                        <div class="text-base text-gray-600 mt-1">Belum Selesai</div>
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
                <div class="report-box zoom-in">
                    <div class="box p-5">
                        <div class="flex">
                            <i data-feather="check" class="report-box__icon text-theme-12"></i>
                        </div>
                        <div class="text-3xl font-bold leading-8 mt-6" id="count_selesai">0</div>
                        <div class="text-base text-gray-600 mt-1">Selesai</div>
                    </div>
                </div>
            </div>
            <div class="col-span-12 sm:col-span-6 xl:col-span-2 intro-y">
                <div class="report-box zoom-in">
                    <div class="box p-5">
                        <div class="flex">
                            <i data-feather="x-circle" class="report-box__icon text-theme-6"></i>
                        </div>
                        <div class="text-3xl font-bold leading-8 mt-6" id="count_batal">0</div>
                        <div class="text-base text-gray-600 mt-1">Batal</div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- BEGIN: Sales Report -->
    <div class="col-span-12 lg:col-span-9 mt-8">
        <div class="intro-y block sm:flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Laporan Penjualan
            </h2>
        </div>
        <div class="intro-y box p-5 mt-12 sm:mt-5">
            <div class="flex flex-col xl:flex-row xl:items-center">
                <div class="flex">
                    <div>
                        <div class="text-theme-20 text-lg xl:text-xl font-bold" id="total_tahun_ini">Rp 0</div>
                        <div class="text-gray-600">Tahun Ini</div>
                    </div>
                    <div class="w-px h-12 border border-r border-dashed border-gray-300 mx-4 xl:mx-6"></div>
                    <div>
                        <div class="text-gray-600 text-lg xl:text-xl font-medium" id="total_tahun_lalu">Rp 0</div>
                        <div class="text-gray-600">Tahun Lalu</div>
                    </div>
                </div>
                <div class="dropdown relative xl:ml-auto mt-5 xl:mt-0">
                    <div class="dropdown-box mt-10 absolute w-40 top-0 xl:right-0 z-20">
                        <div class="dropdown-box__content box p-2 overflow-y-auto h-32"> <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">PC & Laptop</a> <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Smartphone</a> <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Electronic</a> <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Photography</a> <a href="" class="flex items-center block p-2 transition duration-300 ease-in-out bg-white hover:bg-gray-200 rounded-md">Sport</a> </div>
                    </div>
                </div>
            </div>
            <div class="report-chart">
                <canvas id="report-line-chart" height="160" class="mt-6"></canvas>
            </div>
        </div>
    </div>
    <!-- END: Sales Report -->

     <!-- BEGIN: Weekly Top Seller -->
     <div class="col-span-12 sm:col-span-6 lg:col-span-3 mt-8">
        <div class="intro-y flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Penjualan
            </h2>
        </div>
        <div class="intro-y box p-5 mt-5">
            <canvas class="mt-3" id="report-pie-chart" height="280"></canvas>
            <div class="mt-8">
                <div class="flex items-center">
                    <div class="w-2 h-2 bg-theme-11 rounded-full mr-3"></div>
                    <span class="truncate">Belum Terbayar</span>
                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                    <span class="font-medium xl:ml-auto" id="persentase_belum_terbayar">0%</span>
                </div>
                <div class="flex items-center mt-4">
                    <div class="w-2 h-2 bg-theme-1 rounded-full mr-3"></div>
                    <span class="truncate">Terbayar</span>
                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                    <span class="font-medium xl:ml-auto" id="persentase_terbayar">0%</span>
                </div>
                <div class="flex items-center mt-4">
                    <div class="w-2 h-2 bg-theme-12 rounded-full mr-3"></div>
                    <span class="truncate">Selesai</span>
                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                    <span class="font-medium xl:ml-auto" id="persentase_done">0%</span>
                </div>
                <div class="flex items-center mt-4">
                    <div class="w-2 h-2 bg-theme-6 rounded-full mr-3"></div>
                    <span class="truncate">Batal</span>
                    <div class="h-px flex-1 border border-r border-dashed border-gray-300 mx-3 xl:hidden"></div>
                    <span class="font-medium xl:ml-auto" id="persentase_batal">0%</span>
                </div>
            </div>
        </div>
    </div>
    <!-- END: Weekly Top Seller -->

    <div class="col-span-12 mt-4">
        <div class="intro-y block sm:flex items-center h-10">
            <h2 class="text-lg font-medium truncate mr-5">
                Pembelian Terakhir
            </h2>
        </div>
        <div class="intro-y overflow-auto lg:overflow-visible mt-8 sm:mt-0">
            <table class="table table-report sm:mt-2">
                <thead>
                    <tr>
                        <th class="whitespace-no-wrap">No</th>
                        <th class="whitespace-no-wrap">Invoice</th>
                        <th class="whitespace-no-wrap">Customer</th>
                        <th class="whitespace-no-wrap">Alamat</th>
                        <th class="whitespace-no-wrap">Total</th>
                        <th class="whitespace-no-wrap">Status</th>
                        <th class="text-center whitespace-no-wrap">Aksi</th>
                    </tr>
                </thead>
                <tbody>
                    @php
                       $index = 1; 
                    @endphp
                    @foreach ($order as $orders)
                    <tr class="intro-x">
                        <td class="w-10">{{ $index++ }}</td>
                        <td class="w-64">{{ $orders->invoice }}</td>
                        <td class="w-64">{{ ucfirst($orders->customers->nama_lengkap) }}</td>
                        <td class="w-64">{{ $orders->alamat }}</td>
                        <td class="w-64">Rp {{ number_format($orders->total,0,',','.') }}</td>
                        <td class="w-64">
                            @if ($orders->status == 'pending')
                                <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                                    Konfirmasi Pesanan
                                </span>
                            @elseif($orders->status == 'menunggu_pembayaran')
                                <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                                    Menunggu Pembayaran
                                </span>
                            @elseif($orders->status == 'menunggu_persetujuan')
                                <span class="bg-yellow-100 text-yellow-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-yellow-900 dark:text-yellow-300">
                                    Konfirmasi Pembayaran
                                </span>
                            @elseif($orders->status == 'terbayar')
                                <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                                    Sudah terbayar
                                </span>
                            @elseif($orders->status == 'pengiriman')
                                <span class="bg-blue-100 text-blue-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-blue-900 dark:text-blue-300">
                                    Sedang Pengiriman
                                </span>
                            @elseif($orders->status == 'diterima')
                                <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300">
                                    Pesanan Diterima
                                </span>
                            @elseif($orders->status == 'dibatalkan')
                                <span class="bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300">
                                    Dibatalkan
                                </span>
                            @endif
                        </td>
                        <td class="table-report__action w-16">
                            <div class="flex justify-center items-center">
                                <a class="flex items-center mr-3 text-theme-3" href="{{ route('order.show',$orders->id) }}"> <i data-feather="alert-circle" class="w-4 h-4 mr-1"></i> Detail </a>

                            </div>
                        </td>
                    </tr>
                    @endforeach
                </tbody>
            </table>
        </div>
    </div>
  </div>
@endsection
