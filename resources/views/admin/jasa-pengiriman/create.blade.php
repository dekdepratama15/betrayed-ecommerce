@extends('admin.layouts.app')

@section('title')
    Jasa Pengiriman
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Tambah Data Jasa Pengiriman</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('jasa-pengiriman.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('jasa-pengiriman.store') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @csrf
      <div class="grid grid-cols-12 gap-5">
            <div class="col-span-12">
                <div>
                    <label>Nama Jasa</label>
                    <input type="text" name="nama" class="input w-full border mt-2" placeholder="Masukkan nama jasa pengiriman" value="" required >
                </div>
            </div>
            <div class="col-span-12">
                <div>
                    <label>Harga</label>
                    <input type="text" name="harga" class="input w-full border mt-2" placeholder="Masukkan harga jasa pengiriman" value="" required data-format_rupiah="formatRupiah" >
                </div>
            </div>
            <div class="col-span-12 flex justify-end" >
                <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
            </div>
      </div>
    </form>
  </div>
@endsection
