@extends('admin.layouts.app')

@section('title')
    Produk
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Detail Produk</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('product.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-6">
              <div>
                  <label>Nama Produk</label>
                  <input type="text" id="input-produk" name="nama_produk" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Masukkan nama produk" value="{{ ucfirst($data->nama_produk) }}" readonly>
              </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Slug</label>
                <input type="text" id="slug" name="slug" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Slug Produk" value="{{ $data->slug }}" readonly>
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Kategori Produk</label>
                <input type="text" id="kategori" name="category_id" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Kategori Produk" value="{{ ucfirst($data->categories->name) }}" readonly>
            </div>
          </div>
          <div class="col-span-6">
            <div>
                <label>Harga Produk</label>
                <input type="text" name="price" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Masukkan harga" value="Rp {{ number_format($data->price,0,',','.') }}" readonly>
            </div>
          </div>
          
          <div class="col-span-12">
            <div>
              <label>Keywords</label>
              <div>
              @foreach (explode(',',$data->keyword) as $item)
                <span class="bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300" readonly>{{ ucfirst($item) }}</span>
                @endforeach
              </div>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Status Tampil</label>
                <input type="text" name="status" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Masukkan status" value="{{ ucfirst($data->status) }}" readonly>
            </div>
          </div>
          <div class="col-span-12">
            <div class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <h3 class="px-5 pt-5 font-bold text-lg">Data Variant</h3>
              <div class="p-5" id="wrapper-variant">
                @foreach ($variant as $variants)
                <div class="w-full bg-white rounded-lg shadow dark:bg-gray-800 border-black mt-2 p-4">
                  <div class="grid grid-cols-12 gap-5" id="body-variant" style="padding: 5px">
                      <div class="col-span-6">
                        <div>
                            <label>Variant Produk </label>
                            <input type="text" name="status" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Masukkan status" value="{{ ucfirst($variants->nama_variant) }}" readonly>
                        </div>
                      </div>
                      <div class="col-span-6">
                        <div>
                            <label>Stok Produk </label>
                            <input type="text" name="status" class="input w-full border mt-2 mt-2 bg-gray-300" placeholder="Masukkan status" value="{{ ucfirst($variants->stok) }} pcs" readonly>
                        </div>
                      </div>
                    </div>
                    <div class="grid grid-cols-12 gap-5" id="body-variant" style="padding: 5px">
                      <div class="col-span-6">
                        <div class="">
                          <a class="fancybox" id="basic-addon2" data-caption="{{ucfirst($data->nama_produk)}} - {{ucfirst($variants->nama_variant)}}" href="{{asset('upload/'.$variants->img)}}">
                            <img src="{{asset('upload/'.$variants->img)}}" style="width: 150px; height:150px; border-radius:7px; margin: 0 3px; " alt="">
                          </a>
                        </div>
                      </div>
                    </div>
                </div>
                    @endforeach
              </div>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Deskripsi Produk</label>
                <textarea type="text" name="deskripsi" rows="5" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan deskripsi produk" value="" readonly>{{ ucfirst($data->deskripsi) }}</textarea>
            </div>
          </div>
      </div>
    </form>
  </div>
@endsection
