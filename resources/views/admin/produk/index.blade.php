@extends('admin.layouts.app')

@section('title')
    Produk
@endsection

@section('content')
<div class="col-span-12 my-6">
  <div class="flex justify-content-between justify-between">
      <h2 class="text-lg font-medium truncate">Data List Produk</h2>
  </div>
  <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center mt-2">
      <div class="w-full sm:w-auto mt-3 sm:mt-0 sm:ml-auto md:ml-0">
          <div class="w-56 relative text-gray-700">
              <form action="" method="get" class="position-relative">
                  <input type="text" value="{{ $search }}" name="search" autocomplete="off" class="input w-56 box pr-10 placeholder-theme-13" placeholder="Search...">
                  <i class="w-4 h-4 absolute my-auto inset-y-0 mr-3 right-0" data-feather="search"></i> 
              </form>
          </div>
      </div>
      <a href="{{route('product.create')}}" class="button w-36 mb-2 mr-2 mt-2 flex items-center justify-center bg-theme-1 text-white"> <i data-feather="plus" class="w-4 h-4 mr-2"></i> Tambah Data </a>
  </div>
</div>
<!-- BEGIN: Data List -->
<div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
  <table class="table table-report -mt-2">
      <thead>
          <tr>
              <th class="whitespace-no-wrap">No</th>
              <th class="whitespace-no-wrap">Nama Produk</th>
              <th class="whitespace-no-wrap">Kategori Produk</th>
              <th class="whitespace-no-wrap">Harga</th>
              <th class="whitespace-no-wrap">Status</th>
              <th class="text-center whitespace-no-wrap">Aksi</th>
          </tr>
      </thead>
      @php
         $index = 0 
      @endphp
      <tbody>
          @if(count($produks) > 0)
          @foreach ($produks as $data)
          <tr class="intro-x">
              <td class="w-10">{{ ++$index }}</td>
              <td class="w-20">{{ ucfirst($data->nama_produk) }}</td>
              <td class="w-20">{{ ucfirst($data->categories->name) }}</td>
              <td class="w-20">Rp {{ number_format($data->price,0,',','.') }}</td>
              <td class="w-20">
                <span class=" {{$data->status == 'aktif' ? 'bg-green-100 text-green-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-green-900 dark:text-green-300' 
                    : 'bg-red-100 text-red-800 text-xs font-medium me-2 px-3 py-1 rounded dark:bg-red-900 dark:text-red-300'}}
               ">{{ ucfirst($data->status) }}</span>
              </td>
              <td class="table-report__action w-10">
                  <div class="flex justify-center items-center">
                    <a class="flex items-center mr-3" href="{{ route('product.edit', $data->id) }}"> <i data-feather="check-square" class="w-4 h-4 mr-1"></i> Edit </a>
                    <a class="flex items-center mr-3 text-theme-3" href="{{ route('product.show',$data->id) }}"> <i data-feather="alert-circle" class="w-4 h-4 mr-1"></i> Detail </a>
                    <button type="button" class="flex items-center text-theme-6 delete-data" data-redirect="{{route('product.destroy',$data->id)}}" data-id="{{ $data->id }}" data-token="{{ csrf_token() }}"> <i data-feather="trash-2" class="w-4 h-4 mr-1"></i> Hapus </button>
                  </div>
              </td>
          </tr>
          @endforeach
          @else
            <tr>
                <td class="intro-x text-center" colspan="6">Tidak terdapat data Produk</td>
            </tr>
            @endif
      </tbody>
  </table>
  <div class="row">
    <div class="col-12 mt-2 mt-md-4">
        <ul class="pagination pagination_style1 justify-content-center">
           {{ $produks->links() }}
        </ul>
    </div>
</div>
</div>
<!-- END: Data List -->
@endsection