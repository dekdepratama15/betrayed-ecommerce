@extends('admin.layouts.app')

@section('title')
    Produk
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Edit Data Produk</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('product.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('product.update', $data->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @method('PUT')
      @csrf
      <div class="grid grid-cols-12 gap-5">
        <div class="col-span-6">
            <div>
                <label>Nama Produk</label>
                <input type="text" required id="input-produk" name="nama_produk" class="input w-full border mt-2" placeholder="Masukkan nama produk" value="{{ ucfirst($data->nama_produk) }}" >
            </div>
        </div>
        <div class="col-span-6">
          <div>
              <label>Slug</label>
              <input type="text" required id="slug" readonly name="slug" class="input w-full border mt-2" placeholder="Slug Produk" value="{{ $data->slug }}" >
          </div>
        </div>
      <div class="col-span-6">
        <div>
            <label>Kategori Produk</label>
            <select class="input w-full border mt-2" name="category_id" required>
              @foreach ($kategori as $kategoris)
              <option value="{{ $kategoris->id }}" @if($kategoris->id == $data->category_id) selected @endif>{{ ucfirst($kategoris->name) }}</option>
              @endforeach
            </select>
        </div>
      </div>
      <div class="col-span-6">
        <div>
            <label>Harga Produk</label>
            <input type="text" required name="price" class="input w-full border mt-2" placeholder="Masukkan harga" value="{{ number_format($data->price,0,',','.') }}" >
        </div>
      </div>
      <div class="col-span-12">
        <div>
          <label for="">Keywords</label>
          <input type="text" id="tag-input" class="input w-full border mt-2" placeholder="Tambahkan Keywords">
          <input type="hidden" required value="{{ $data->keyword }}" id="tag-value" name="keyword">
          <div id="tag-input-container" class="flex flex-wrap gap-1 mt-2"></div>
        </div>
      </div>
      <div class="col-span-12">
        <div>
            <label>Status Tampil</label>
            <select class="input w-full border mt-2" name="status" required>
              @if ($data->status == 'aktif')
                <option value="{{ $data->status }}">{{ ucfirst($data->status) }}</option>
                <option value="tidak aktif">Tidak Aktif</option>
              @elseif($data->status == 'tidak aktif')
                <option value="aktif">Aktif</option>
                <option value="{{ $data->status }}">{{ ucfirst($data->status) }}</option>
              @endif
            </select>
        </div>
      </div>
      <div class="col-span-12">
        <div class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
          <h3 class="px-5 pt-5 font-bold text-lg">Data Variant</h3>
                  <div id="body-gambar" class="col-span-12">
                    @php
                    $gambar_blog = '';
                    $stok_variant = '';
                    @endphp
                    @foreach ($variant as $key => $value)
                      @php
                        if ($stok_variant != '' ) {
                            $stok_variant .= ',';
                        }
                        $stok_variant .= $value->id;

                        if($gambar_blog != ''){
                        $gambar_blog .= ',';
                        }
                        $gambar_blog .= $key+1;
                        $showDelete = false;
                        if($key > 0){
                        $showDelete = true;
                        }
                      @endphp
                    <div id="pembungkus-{{$key+1 }}" class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-2">
                      <div class="p-5" id="wrapper-variant">
                        <div class="grid grid-cols-12 gap-5">
                          <div class="col-span-4">
                            <div>
                                <label>Variant Produk</label>
                                <input type="hidden" required name="variant_id[]" class="input w-full border mt-2" placeholder="Masukkan variant produk" value="{{ ucfirst($value->id) }}" >
                                <input type="text" required name="nama_variant_update[]" class="input w-full border mt-2" placeholder="Masukkan variant produk" value="{{ ucfirst($value->nama_variant) }}" >
                            </div>
                          </div>
                          <div class="col-span-2">
                            <div>
                                <label>Stok Produk</label>
                                <input type="text" required name="stok_update[]" class="input w-full border mt-2 input-stok-variant" placeholder="Stok" data-id="{{ $value->id }}" value="{{ $value->stok }}" >
                            </div>
                          </div>
                            <div class="col-span-5">
                                <label>Gambar</label>
                                <div class="input-group">
                                    <input type="file" name="img_update[]" class="input w-full border mt-2 input-file-gambar" data-id="{{ $value->id }}" value="" multiple>
                                    <div class="mt-4">
                                      <a class="input-group-text button fancybox bg-theme-1 text-white" id="basic-addon2" data-caption="{{$data->nama_produk}}" href="{{asset('upload/'.$value->img)}}">Lihat Gambar</a>
                                    </div>
                                </div>
                            </div>
                            <div class="col-span-1">
                              @if ($showDelete)
                                <div>
                                  <button
                                    type="button"
                                    class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-red-700 text-white btn-hapus-row"
                                    data-key="{{ $key+1 }}"
                                    data-id="{{ $value->id }}"
                                    style="margin-top: 2em"
                                    >
                                    <i class="fa fa-trash-o" style="width: 20px" aria-hidden="true"></i></button>
                                </div>
                              @else
                                <div>
                                  <button href="" id="btn-add-row" type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-theme-1 text-white" style="margin-top: 2em" ><i class="fa fa-plus" style="width: 20px" aria-hidden="true"></i></button>
                                </div>
                              @endif
                            </div>
                        </div>
                      </div>
                    </div>
                    @endforeach

                    <input type="hidden" id="stok_variant" value="{{ $stok_variant }}" name="stok_variant_edit">
                    <input type="hidden" id="stok_variant_delete" name="stok_variant_delete" value="">
                    <input type="hidden" id="stok_variant_edit" value="">

                    <input type="hidden" id="gambar_blog" value="{{ $gambar_blog }}">
                    <input type="hidden" id="gambar_blog_delete" name="gambar_blog_delete" value="">
                    <input type="hidden" id="gambar_blog_edit" name="gambar_blog_edit" value="">
                  </div>

        </div>
      </div>

      <div class="col-span-12">
        <div>
            <label>Deskripsi Produk</label>
            <textarea type="text" equired name="deskripsi" rows="5" class="input w-full border mt-2" placeholder="Masukkan deskripsi produk" value="" >{{ $data->deskripsi }}</textarea>
        </div>
      </div>
        <div class="col-span-12 flex justify-end" >
          <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
        </div>
      </div>
    </form>
  </div>
@endsection

@section('js-content')
<script>
  $(document).ready(function(){
        $('#input-produk').on('keyup',function(){
          $('#slug').val(createSlug($(this).val()))
        })
      })

      function createSlug(input) {
        return input.toLowerCase().replace(/\s+/g, '-').replace(/[^\w\-]+/g, '').replace(/\-\-+/g, '-');
      }
  $(document).ready(function () {
    var gambar_blogs = $('#gambar_blog').val().split(',');
    var stok_variant = $('#stok_variant').val().split(',');

      $('#btn-add-row').click(function () {
        addRow()
      });
    //   Set Stok Variant
      $('.input-stok-variant').on('keyup',function(e) {
        var stok_variant_edit = $('#stok_variant_edit').val();
        console.log(stok_variant_edit);
        if (stok_variant_edit != '') {
            stok_variant_edit += ',';
        }
        stok_variant_edit += $(this).data('id')
        $('#stok_variant_edit').val(stok_variant_edit)
      })

    //   set Gambar
      $('.input-file-gambar').on('change', function (e) {
        var gambar_blog_edit = $('#gambar_blog_edit').val();
        if (gambar_blog_edit != "") {
          gambar_blog_edit += ',';
        }
          gambar_blog_edit += $(this).data('id')
        $('#gambar_blog_edit').val(gambar_blog_edit);
      })

      // Hapus Baris
      $('#body-gambar').on('click', '.btn-hapus-row', function () {
          var gambar_blog_delete = $('#gambar_blog_delete').val();
          var stok_variant_delete = $('#stok_variant_delete').val();

          if (stok_variant_delete != "") {
            stok_variant_delete += ',';
        }
        $('#stok_variant_delete').val(gambar_blog_delete);

          if (gambar_blog_delete != "") {
            gambar_blog_delete += ',';
        }
        gambar_blog_delete += $(this).data('id')
        $('#gambar_blog_delete').val(gambar_blog_delete);
          $('#pembungkus-'+$(this).data('key')).remove();
      });
  });

  function addRow() {
    var jumlah = $('input[type="file"]');
    var newRow = `
      <div id="pembungkus-${jumlah.length + 1}" class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-2">
        <div class="p-5">
          <div class="grid grid-cols-12 gap-5" >
      <div class="col-span-4">
        <div>
            <label>Variant Produk</label>
            <input type="text" required name="nama_variant[]" class="input w-full border mt-2" placeholder="Masukkan variant produk" value="" >
        </div>
      </div>
      <div class="col-span-2">
        <div>
            <label>Stok Produk</label>
            <input type="number" required name="stok[]" class="input w-full border mt-2" placeholder="Stok" value="" >
        </div>
      </div>
              <div class="col-span-5">
                <div>
                    <label>Gambar</label>
                    <input type="file" name="img[]" class="input w-full border mt-2" value="" multiple required>
                </div>
              </div>
              <div class="col-span-1">
                <div>
                  <button data-key="${jumlah.length + 1}" type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-red-700 text-white btn-hapus-row-new" style="margin-top: 2em" ><i class="fa fa-trash-o" style="width: 20px" aria-hidden="true"></i></button>
                </div>
              </div>
          </div>
        </div>
    </div>
      `
      $('#body-gambar').append(newRow);
      $('#body-gambar').on('click', '.btn-hapus-row', function () {
          var gambar_blog_delete = $('#gambar_blog_delete').val();
          var stok_variant_delete = $('#stok_variant_delete').val();

          if (stok_variant_delete != "") {
            stok_variant_delete += ',';
        }
        $('#stok_variant_delete').val(gambar_blog_delete);

          if (gambar_blog_delete != "") {
            gambar_blog_delete += ',';
        }
        gambar_blog_delete += $(this).data('id')
        $('#gambar_blog_delete').val(gambar_blog_delete);
          $('#pembungkus-'+$(this).data('key')).remove();
      });
  }
</script>

<script>
  $(document).ready(function() {
    const tagInputContainer = $('#tag-input-container');
    const tagInput = $('#tag-input');
    const tagValue = $('#tag-value');
      var keyword = '{{ $data->keyword }}'
      $.each(keyword.split(','), function (key, val) {
        addTag(val)
      })

    function addTag(tag) {
      const tagElement = $('<div>', {
        class: 'bg-blue-500 text-white rounded px-2 py-1 flex items-center mr-2 mb-2',
      });

      const tagText = $('<span>', {
        text: tag,
      });

      const removeButton = $('<button>', {
        class: 'ml-2 focus:outline-none',
        html: '&times;',
        click: function() {
          tagElement.remove();
          updateTagValue();
        },
      });

      tagElement.append(tagText, removeButton);
      tagInputContainer.append(tagElement);
      updateTagValue();
    }

    function updateTagValue() {
      const tags = [];
      tagInputContainer.find('.bg-blue-500 span').each(function() {
        tags.push($(this).text());
      });
      tagValue.val(tags.join(','));
    }

    tagInput.on('input', function() {
      const inputValue = tagInput.val().trim();
      tagValue.val(inputValue);
    });

    tagInput.on('keypress', function(event) {
      if (event.which === 13) {
        event.preventDefault();
        handleTagInput();
      }
    });

    function handleTagInput() {
      const inputValue = tagInput.val().trim();

      if (inputValue !== '') {
        addTag(inputValue);
        tagInput.val('');
      }
    }
  });
</script>

@if(session('error'))

  <script>
  $(document).ready(function() {
      Swal.fire(
          'Gagal!',
          '{{session("error")}}',
          'warning'
      );
  });
  </script>
  @endif

@endsection
