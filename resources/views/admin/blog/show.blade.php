@extends('admin.layouts.app')

@section('title')
    Blog
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Detail Blog</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('blog.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-12">
              <div>
                  <label>Judul Blog</label>
                  <input type="text" name="judul" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan judul blog" value="{{ ucfirst($blog->judul) }}" >
              </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Deskripsi</label>
                <textarea type="text" rows="5" readonly name="deskripsi" class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan Deskripsi" value="" >{{ ucfirst($blog->deskripsi) }}</textarea>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Status</label>
                <input type="text" name="judul" readonly class="input w-full border mt-2 bg-gray-300" placeholder="Masukkan judul blog" value="{{ ucfirst($blog->status) }}" >
            </div>
          </div>
          <label for="">Gambar</label>
          <div class="col-span-12 flex flex-row gap-2">
            @foreach ($media as $key => $value)
            <div class="">
              <a class="fancybox" id="basic-addon2" data-caption="{{$blog->judul}}" href="{{asset('upload/'.$value->media)}}">
                <img src="{{asset('upload/'.$value->media)}}" style="width: 180px; height:180px; border-radius:7px; margin: 0 3px; " alt="">
              </a>
            </div>
            @endforeach
          </div>
      </div>
  </div>
@endsection
