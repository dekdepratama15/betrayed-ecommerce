@extends('admin.layouts.app')

@section('title')
    Blog
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Edit Data Blog</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('blog.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('blog.update', $data->id) }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @method('PUT')
      @csrf
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-12">
              <div>
                  <label>Judul Blog</label>
                  <input type="text" name="judul" class="input w-full border mt-2" placeholder="Masukkan judul blog" value="{{ ucfirst($data->judul) }}" >
              </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Deskripsi</label>
                <textarea type="text" name="deskripsi" class="input w-full border mt-2" placeholder="Masukkan Deskripsi" value="" >{{ ucfirst($data->deskripsi) }}</textarea>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Status</label>
                <select 
                  class="input w-full border mt-2"
                  name="status"
                >
                @if ($data->status == 'publish')
                  <option value="{{ $data->status }}" selected>{{ ucfirst($data->status) }}</option>
                  <option value="non publish">Non Publish</option>
                @else
                <option value="publish">Publish</option>
                <option value="{{ $data->status }}" selected>{{ ucfirst($data->status) }}</option>
                @endif
                </select>
            </div>
          </div>
          <div id="body-gambar" class="col-span-12">
            @php
            $gambar_blog = '';
            @endphp
            @foreach ($media as $key => $value)
              @php
                  if($gambar_blog != ''){
                    $gambar_blog .= ',';
                  }
                  $gambar_blog .= $key+1;
                  $showDelete = false;
                  if($key > 0){
                    $showDelete = true;
                  }
              @endphp
            <div id="pembungkus-{{$key+1 }}" class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-2">
              <div class="p-5" id="wrapper-variant">
                <div class="grid grid-cols-12 gap-5" id="body-variant">
                    <div class="col-span-11">
                        <label>Gambar</label>
                        <div class="input-group">
                            <input type="file" name="img_update[]" class="input w-full border mt-2 input-file-gambar" data-id="{{ $value->id }}" value="" multiple>
                            <div class="mt-4">
                              <a class="input-group-text button fancybox bg-theme-1 text-white" id="basic-addon2" data-caption="{{$data->judul}}" href="{{asset('upload/'.$value->media)}}">Lihat Gambar</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-span-1">
                      @if ($showDelete)
                        <div>
                          <button 
                            type="button" 
                            class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-red-700 text-white btn-hapus-row" 
                            data-key="{{ $key+1 }}"
                            data-id="{{ $value->id }}"
                            style="margin-top: 2em" 
                            > 
                            <i class="fa fa-trash-o" style="width: 20px" aria-hidden="true"></i></button>
                        </div>
                      @else
                        <div>
                          <button href="" id="btn-add-row" type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-theme-1 text-white" style="margin-top: 2em" ><i class="fa fa-plus" style="width: 20px" aria-hidden="true"></i></button>
                        </div>
                      @endif  
                    </div>
                </div>
              </div>
            </div>
            @endforeach
            <input type="hidden" id="gambar_blog" value="{{ $gambar_blog }}">
            <input type="hidden" id="gambar_blog_delete" name="gambar_blog_delete" value="">
            <input type="hidden" id="gambar_blog_edit" name="gambar_blog_edit" value="">
          </div>
          <div class="col-span-12 flex justify-end" >
            <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
          </div>
      </div>
    </form>
  </div>
@endsection

@section('js-content')
<script>
  $(document).ready(function () {
    var gambar_blogs = $('#gambar_blog').val().split(',');
    console.log(gambar_blogs);
      // Tambah Baris Baru

      $('#btn-add-row').click(function () {
        addRow()  
      });

      $('.input-file-gambar').on('change', function (e) {
        var gambar_blog_edit = $('#gambar_blog_edit').val();
        if (gambar_blog_edit != "") {
          gambar_blog_edit += ',';
        }
          gambar_blog_edit += $(this).data('id')
        $('#gambar_blog_edit').val(gambar_blog_edit);
      })

      // Hapus Baris
      $('#body-gambar').on('click', '.btn-hapus-row', function () {
          var gambar_blog_delete = $('#gambar_blog_delete').val();
          if (gambar_blog_delete != "") {
            gambar_blog_delete += ',';
          }
            gambar_blog_delete += $(this).data('id')
          $('#gambar_blog_delete').val(gambar_blog_delete);
          $('#pembungkus-'+$(this).data('key')).remove();
      });
  });

  function addRow() {
    var jumlah = $('input[type="file"]');
    var newRow = `
    <div id="pembungkus-${jumlah.length + 1}" class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700 mt-2">
      <div class="p-5" id="wrapper-variant">
        <div class="grid grid-cols-12 gap-5" id="body-variant">
            <div class="col-span-11">
              <div>
                  <label>Gambar</label>
                  <input type="file" name="img[]" class="input w-full border mt-2" value="" multiple>
              </div>
            </div>
            <div class="col-span-1">
              <div>
                <button data-key="${jumlah.length + 1}" type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-red-700 text-white btn-hapus-row-new" style="margin-top: 2em" ><i class="fa fa-trash-o" style="width: 20px" aria-hidden="true"></i></button>
              </div>
            </div>
        </div>
      </div>
    </div>
      `
      $('#body-gambar').append(newRow);
      $('#body-gambar').on('click', '.btn-hapus-row-new', function () {
          $('#pembungkus-'+$(this).data('key')).remove();
      });
  }
</script>
@endsection
