@extends('admin.layouts.app')

@section('title')
    Blog
@endsection

@section('content')
  <div class="col-span-12 my-6">
    <div class="flex justify-between items-center">
        <h2 class="text-lg font-medium truncate">Tambah Data Blog</h2>
        <div class="intro-y col-span-12 flex justify-between flex-wrap sm:flex-no-wrap items-center">
            <a href="{{route('blog.index')}}" class="button w-36 mb-2 flex items-center justify-center bg-gray-700 text-white"> <i data-feather="arrow-left" class="w-4 h-4 mr-2"></i> Kembali </a>
        </div>
    </div>
  </div>
  <div class="intro-y col-span-12 overflow-auto lg:overflow-visible">
    <form action="{{ route('blog.store') }}" method="POST" autocomplete="off" enctype="multipart/form-data">
      @csrf
      <div class="grid grid-cols-12 gap-5">
          <div class="col-span-12">
              <div>
                  <label>Judul Blog</label>
                  <input type="text" required name="judul" class="input w-full border mt-2" placeholder="Masukkan judul blog" value="" >
              </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Deskripsi</label>
                <textarea type="text" required name="deskripsi" class="input w-full border mt-2" placeholder="Masukkan Deskripsi" value="" ></textarea>
            </div>
          </div>
          <div class="col-span-12">
            <div>
                <label>Status</label>
                <select
                  class="input w-full border mt-2"
                  name="status"
                  required
                >
                  <option disabled selected>Pilih Status</option>
                  <option value="publish">Publish</option>
                  <option value="non publish">Tidak Publish</option>
                </select>
            </div>
          </div>
          <div class="col-span-12">
            <div class="w-full bg-white rounded-lg shadow dark:bg-gray-800 dark:border-gray-700">
              <div class="p-5" id="wrapper-variant">
                <div class="grid grid-cols-12 gap-5" id="body-variant">
                    <div class="col-span-11">
                      <div>
                          <label>Gambar</label>
                          <input type="file" name="img[]" required class="input w-full border mt-2" value="" multiple>
                      </div>
                    </div>
                    <div class="col-span-1">
                      <div>
                        <button href="" id="btn-add-row" type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-theme-1 text-white" style="margin-top: 2em" > <i data-feather="plus" class="w-4 h-4"></i></button>
                      </div>
                    </div>
                </div>
              </div>
            </div>
          </div>
          <div class="col-span-12 flex justify-end" >
            <button type="submit" class="button w-36 mb-2 flex items-center justify-center bg-theme-1 text-white"> Simpan </button>
          </div>
      </div>
    </form>
  </div>
@endsection

@section('js-content')
    <script>
      $(document).ready(function(){
        $('#btn-add-row').on('click',function(){
          addVariant();
        })
      })

      function addVariant(){
        var row = `
          <div class="grid grid-cols-12 gap-5" id="body-variant">
              <div class="col-span-11 mt-4">
                <div>
                  <label>Gambar</label>
                  <input type="file" name="img[]" class="input w-full border mt-2" value="" multiple>
                </div>
              </div>
              <div class="col-span-1">
                <div>
                  <button type="button" class="button w-36 mb-2 mr-2 flex h-10 items-center justify-center bg-red-700 text-white btn-hapus-row" style="margin-top: 47px" ><i class="fa fa-trash-o" style="width: 20px" aria-hidden="true"></i></button>
                </div>
              </div>
          </div>
        `
        $('#wrapper-variant').append(row)
        $('#wrapper-variant').on('click','.btn-hapus-row',function(){
        $(this).closest('#body-variant').remove();
        })
      }
    </script>
@endsection
