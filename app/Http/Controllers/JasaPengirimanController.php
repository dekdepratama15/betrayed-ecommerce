<?php

namespace App\Http\Controllers;

use App\Models\JasaPengiriman;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class JasaPengirimanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $search = $request->search;
        $jasapengirimans = JasaPengiriman::where('id', '!=', 0);
        if (isset($search) && !empty($search)) {
            $jasapengirimans->where('nama', 'like', '%' . $search . '%');
        }

        $jasapengirimans = $jasapengirimans->paginate(10);
        return view('admin.jasa-pengiriman.index',[
            'jasapengirimans' => $jasapengirimans,
            'search' => $search
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.jasa-pengiriman.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required',
            'harga' => 'required'
        ]);
        try {
            DB::beginTransaction();
            $jasapengiriman = new JasaPengiriman();
            $jasapengiriman->nama = $request->nama;
            $jasapengiriman->harga = $this->numberFormat($request->harga);
            $jasapengiriman->save();
            DB::commit();
            session()->flash('success','Berhasil Menambahkan Data Jasa Pengiriman');
            return redirect()->route('jasa-pengiriman.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('warning','Gagal Menambahkan Data Jasa Pengiriman!');
            return redirect()->back();
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\JasaPengiriman  $jasaPengiriman
     * @return \Illuminate\Http\Response
     */
    public function show(JasaPengiriman $jasaPengiriman)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\JasaPengiriman  $jasaPengiriman
     * @return \Illuminate\Http\Response
     */
    public function edit(JasaPengiriman $jasaPengiriman)
    {
        return view('admin.jasa-pengiriman.edit',[
            'jasaPengiriman' => $jasaPengiriman
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\JasaPengiriman  $jasaPengiriman
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, JasaPengiriman $jasaPengiriman)
    {
        try {
            DB::beginTransaction();
            $jasaPengiriman->nama = $request->nama;
            $jasaPengiriman->harga = $this->numberFormat($request->harga);
            $jasaPengiriman->save();
            DB::commit();
            session()->flash('success','Berhasil Mengubah Data Jasa Pengiriman');
            return redirect()->route('jasa-pengiriman.index');
        } catch (\Exception $e) {
            DB::rollBack();
            session()->flash('warning','Gagal Mengubah Data Jasa Pengiriman!');
            return redirect()->back();
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\JasaPengiriman  $jasaPengiriman
     * @return \Illuminate\Http\Response
     */
    public function destroy(JasaPengiriman $jasaPengiriman)
    {
        JasaPengiriman::destroy($jasaPengiriman->id);
        return response()->json([
            'message' => 'Berhasil Menghapus Data Pengiriman'
        ]);
    }
}
