<?php

namespace App\Http\Controllers;

use App\Models\customers;
use App\Models\orders;
use App\Models\produks;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class DashboardAdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $produk     = produks::all();
        $customer   = customers::all();
        $order      = orders::orderBy('id', 'DESC')->limit(5)->get();
        

        // $orderTerbayar = orders::where('status','terbayar')->get();
        // $countOrderTerbayar = $orderTerbayar->count();
        // $persenPending = ($countOrderPending / $countOrder) * 100;
        // $persenTerbayar = ($countOrderTerbayar / $countOrder) * 100;
        // $persenSukses = ($countOrderSukses / $countOrder) * 100;
        // $penjualanBulanLalu = orders::where('status','diterima')
        //                     ->whereYear('created_at', '=', now()->subMonth()->year)
        //                     ->whereMonth('created_at', '=', now()->subMonth()->month)
        //                     ->sum('total');
        // $penjualanBulanIni = orders::where('status','diterima')
        //                     ->whereYear('created_at', '=', now()->year)
        //                     ->whereMonth('created_at', '=', now()->month)
        //                     ->sum('total');
        return view('admin.dashboard',[
            'produk' => count($produk),
            'customer' => count($customer),
            'order' => $order,
            // 'orderTerbayar' => $countOrderTerbayar,
            // 'persenPending' => $persenPending,
            // 'persenTerbayar' => $persenTerbayar,
            // 'persenSukses' => $persenSukses,
            // 'penjualanBulanLalu' => $penjualanBulanLalu,
            // 'penjualanBulanIni' => $penjualanBulanIni
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroyAdmin()
    {
        Auth::logout();

        return redirect()->route('login');
    }

    function getPenjualan() {
        $order = orders::all();
        $done = orders::where('status','diterima')->get();
        $batal = orders::where('status','dibatalkan')->get();
        $belum_terbayar = orders::whereIn('status',['pending', 'menunggu_pembayaran', 'menunggu_persetujuan'])->get();
        $terbayar = orders::whereIn('status',['terbayar', 'pengiriman'])->get();
        $persentase_done = (count($done) / count($order)) * 100;
        $persentase_belum_terbayar = (count($belum_terbayar) / count($order)) * 100;
        $persentase_terbayar = (count($terbayar) / count($order)) * 100;
        $persentase_batal = (count($batal) / count($order)) * 100;


        return response()->json([
            'persentase_done' => $persentase_done,
            'persentase_belum_terbayar' => $persentase_belum_terbayar,
            'persentase_terbayar' => $persentase_terbayar,
            'persentase_batal' => $persentase_batal,
            'count_done' => count($done),
            'count_belum_terbayar' => count($belum_terbayar),
            'count_terbayar' => count($terbayar),
            'count_batal' => count($batal),
        ]);
    }

    function getPenjualanBulan() {
        $tahun_lalu = orders::whereIn('status',['terbayar', 'pengiriman', 'diterima'])
                            ->whereYear('created_at', '=', (now()->year - 1))
                            ->get();
        $tahun_ini = orders::whereIn('status',['terbayar', 'pengiriman', 'diterima'])
                            ->whereYear('created_at', '=', now()->year)
                            ->get();
        $group_tahun_lalu = [];
        $total_tahun_lalu = 0;
        foreach ($tahun_lalu as $key => $value) {
            if (isset($group_tahun_lalu[date('M', strtotime($value->created_at))]) && !empty($group_tahun_lalu[date('M', strtotime($value->created_at))])) {
                $group_tahun_lalu[date('M', strtotime($value->created_at))] += $value->total;
            } else {
                $group_tahun_lalu[date('M', strtotime($value->created_at))] = $value->total;
            }
            $total_tahun_lalu += $value->total;
        }

        $group_tahun_ini = [];
        $total_tahun_ini = 0;
        foreach ($tahun_ini as $key => $value) {
            if (isset($group_tahun_ini[date('M', strtotime($value->created_at))]) && !empty($group_tahun_ini[date('M', strtotime($value->created_at))])) {
                $group_tahun_ini[date('M', strtotime($value->created_at))] += $value->total;
            } else {
                $group_tahun_ini[date('M', strtotime($value->created_at))] = $value->total;
            }
            $total_tahun_ini += $value->total;
        }
        
        return response()->json([
            'group_tahun_lalu' => $group_tahun_lalu,
            'group_tahun_ini' => $group_tahun_ini,
            'total_tahun_lalu' => $total_tahun_lalu,
            'total_tahun_ini' => $total_tahun_ini,
        ]);
    }
}
